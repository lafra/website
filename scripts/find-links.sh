#!/bin/bash
#
# Trova i link interni nei files .html contenuti in una directory
# (e relative sottodirectory).
#

# Versione che guarda ai file HTML:
#find "${1:-public}" -type f -name '*.html' \
#  | xargs -n 1 perl -ne 'while(/href="([^"]+)"/g){print "$1\n";}' \
#  | grep -v '^#' \
#  | grep -Ev '^mailto:' \
#  | sed -e 's/#.*$//' \
#  | sort | uniq

# Versione che guarda ai file Markdown:
find "${1:-src}" -type f -name '*.md' \
  | xargs -n 1 perl -ne 'while(/\[[^\]]*\]\(([^\) ]*)( "[^"]+")?\)/g){print "$1\n";}' \
  | grep -v '^#' \
  | grep -Ev '^mailto:' \
  | sed -e 's/#.*$//' \
  | sort | uniq


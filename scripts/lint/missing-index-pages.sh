#!/bin/bash
#
# Trova directory che non hanno una pagina 'index'.
#

for d in $(find src -type d) ; do
  test -e ${d}/index.en.md || echo "${d} has no index page" >&2
done

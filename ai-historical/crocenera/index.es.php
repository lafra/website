<html>
<head>
  <title>Cierre preventivo del correo de Crocenera</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Sequestro preventivo crocenera filiarmonici"/>
  <meta name="keywords" content="Sequestro preventivo crocenera filiarmonici"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <h3>Sequestro preventivo Crocenera</h3>
  <small>Documentazione sul sequestro preventivo della casella croceneraanarchica@inventati.org</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian</a> | <a href="index.en.php">english</a> | <a href="index.es.php">spanish</a>]<br/>
  <hr/>

  <ul>
   <li>Comunicati<br>
	<ul>
	<li>
   	<a href="http://italy.indymedia.org/news/2005/05/799042.php?xs=y">
	Comunicato filiarmonici</a>
	</li>
	<li>
   	<a href="materiali/comunicato_ecn.html">
	Comunicato Isole nella Rete</a> [<a href="http://www.ecn.org">ECN</a>]
	</li>




	</ul>
   </li>
  </ul>



  <hr/>

  <ul>
   <li>Mirror Filarmonici Web Site<br>
	<ul>
	<li>
   	<a href="http://web.archive.org/web/20041020053714/http://www.filiarmonici.org/crocenera.html">
	web.archive.org</a>
	</li>
	<li>
   	<a href="http://squat.net/filiarmonici/crocenera/">
	squat.net</a>
	</li>
	<li>
   	<a href="http://riseup.net/filiarmonici/crocenera.html">
	riseup.net</a>
	</li>


	</ul>
   </li>
  </ul>



  <hr/>

  <ul>
   <li>Dibattito sulle intercettazioni in Italia<br>
	<ul>
	<li>
	<a href="http://punto-informatico.it/p.asp?i=52273">
	Sotto l'occhio del grande orecchio<br>
	</a>
	</li>
	<li>
	<a href="http://punto-informatico.it/p.asp?i=51648">
	Intercettazioni, gli operatori si scaldano<br>
	</a>
	</li>
	<li>
	<a href="http://www.spialaspia.org">
	Spia la Spia<br>
	</a>
	</li>
	
	


</ul>
  </ul>
  <hr/>

  <a href="http://www.inventati.org">Home Page del progetto Autistici/Inventati</a><br/>

 </div>


 <div id="main">




<h2>
Cierre preventivo del correo de Crocenera
</h2>

<br>
<br>
<h2><b>Comunicato Autistici/Inventati del 27/05/2005</b></h2>
<p>
La ma�ana del 26 de mayo hemos recibido un fax y diversas llamadad 
telef�nicas de la Digos solicitando la clausura de una direcci�n de 
correo albergado en nuestro server. Se trata del mail
</p>
<p>
croceneraanarchica(at)inventati.org
</p>
<p>

Al mismo tiempo ha sido solicitado el cierre preventivo (="sequestro") 
de la p�gina
</p>
<p>
   <a href="http://www.filiarmonici.org/crocenera.html">
   http://www.filiarmonici.org/crocenera.html
   </a>
<br />

<strong> Filarmonici Mirror Web Site on: </strong>
<a href="http://squat.net/filiarmonici/crocenera/">Squat.net</a> </p>
<p>
La solicitud se enmarca en el �mbito de los arrestos y registros 
sucedidos el mismo d�a contra diversas personas un poco por toda Italia. 
Para mayor informaci�n pod�is consultar
</p>
<p>
   <a href="http://italy.indymedia.org/news/2005/05/798576.php">
   http://italy.indymedia.org
   </a>
</p>
<p>
o entre otros:
</p>
<p>
   <a href="http://www.anarcotico.net">
   http://www.anarcotico.net
   </a>
</p>
<p>
Hemos sido obligados a borrar el correo, como solicitado en la orden que 
pod�is leer en
</p>
<p>
   <a href="http://italy.indymedia.org/news/2005/05/798576.php">
   http://italy.indymedia.org/news/2005/05/798576.php
   </a>
</p>
<p>
Es la primera vez que como servidor autogestionado recibimos una 
solicitud de este tipo. De hecho no hemos sido obligados a revelar datos 
relativos al uso de un cierto correo electr�nico como en otras 
ocasiones, sino a borrar la cuenta. En cualquier cosa no hubieramos 
podido revelar informaci�n alguna de este tipo, dada la orientaci�n 
no-log de nuestras m�quinas: no mantenemos datos sensibles sobre el uso 
de los recursos de autistici/inventati y por tanto no tenemos la 
capacidad de asociar una cuenta de correo o de web a una persona f�sica. 
De los actos se comprende de todas maneres que el seguimiento de las 
comunicaciones por parte de los investigadores se refina con el 
ejercicio. El n�mero de interceptaciones de correo no es controlable y 
envuelve siempre m�s a menudo los grandes proveedores comerciales, en el 
caso espec�fico hotmail.com. Un proveedor comercial com�n evidentemente 
no siente la necesidad de garantizar la reserva de la correspondencia de 
los propios usuarios. Este dato se ha visto claramente tambi�n en el 
�mbito de la investigaci�n de Cosenza
</p>
<p>
   <a href="http://www.autistici.org/it/stuff/archive/newsletter/20021119-comunicato-arresti-cosenza.html">
   http://www.autistici.org/it/stuff/archive/newsletter/20021119-comunicato-arresti-cosenza.html
  </a>
<p>
donde parte del material citado hab�a sido conseguido por medio de la 
intervenci�n sobre cuentas de correo hospedadas por proveedores 
comerciales. La necesidad/derecho de privacidad y anonimato son unos de 
los puntos fundamentales de nuestro proyecto y en nuestra opini�n 
inseparable de cualquier forma de comunicaci�n.

M�s all� de este caso espec�fico pensamos que es verdaderamente 
preocupante la utilizaci�n cada vez m�s desenvuelta que magistratura y 
cuerpos de polic�a hacen de interceptaciones, registros y cierres. Hace 
unos meses, precisamente el 25 de febrero, en algunos titulares 
period�sticos, y en particular en el "sole 24 ore", aparec�an art�culos 
en los cuales Telecom se lamentaba de no tener suficientes recursos 
t�cnicos para satisfacer todas las solicitudes de intervenciones 
telef�nicas de la magistratura. Se trata de un hecho interesante, al 
l�mite del rid�culo, pero que aclara la dimensi�n del probelam. No somos 
por tanto solo nosotros, paranoicos de vocaci�n, los que nos damos 
cuenta de la anomal�a. El mercado de las intervenciones est� en plena 
expansi�n: se habla de 300 millones de euros al a�o para el 2004 y de 
algo del tipo de 140 mil intervenciones sobre m�viles solamente para la 
Telecom y cerca de 120 mil listas de llamadas cedidas a la magistratura. 
Son datos que no nos hemos inventado, sino que hemos recogido de un 
especial de "repubblica", uno de tantos �rganos de "informaci�n" que 
est� contribuyendo en estos d�as al linchamiento medi�tico de los 
detenidos y de los registros. Se trata de un especial sobre 
intervenciones telef�nicas consultable on line:

<a href="http://www.repubblica.it/2004/k/sezioni/cronaca/intercett/intercett/intercett.html">Repubblica On-line</a>
<br /> 

Analogamente a cuanto sucede en la "vida real", est� cada vez m�s lejos 
la idea de una red libre y incensurable. Donde existe quien tiene el 
poder de hojear nuestras vidas como un libro abierto y lo aprovecha cada 
vez m�s; y con cada vez mayor conocimiento de causa, nosotros nos 
sentimos cada vez menos libres, pero cada vez m�s decididos a defender y 
ampliar nuestros espacios de libertad con las u�as y con los dientes. 
Estas pocas l�neas en caliente son las primeras que nos atrevemos a 
publicar. Para una lectura m�s amplia de lo sucedido os remitimos al 
pr�ximo comunicado conjunto con Isole nella rete.
</p>


  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    29/09/2010   </em></small>
  </p>


 </div>

</body>
</html>

<html>
<head>
  <title>Documentazione legale</title>
  <link rel="stylesheet" type="text/css" href="ti.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  <meta http-equiv="Content-Language" content="it"/>
  <meta name="description" content="Citazione della compagnia ferroviaria italiana contro l'associazione Autistici Inventati. Raccolta atti giudiziari del processo per la chiusura di un sito di satira. Case Study."/>
  <meta name="keywords" content="Clienti Insoddisfatti, Sindacato ferrotranvieri, Censura contro la satira, diritto alla satira. sito censurato, guerra in iraq, trasporto armi, trasporto materiale bellico, liberta' di espressione, boicottare, pendolari, orario dei treni"/>
  <meta name="revisit" content="1 day"/>
  <meta name="robots" content="index,follow"/>
</head>


<body>

 <div id="banner">
  <img align="right" clear="none" src="materiali/banner_ai_half.png" alt="banner autistici/inventati sotto attacco"/>
  <h3>E' arrivato un vagone carico di...</h3>
  <small>Documentazione sulla citazione da parte di Trenitalia contro Autistici/Inventati</small>
 </div>



 <div id="sidebar">

  [<a href="index.php">italian version</a>] -  [<a href="index.en.php">english version</a>]<br/>

  <hr/>

  <ul>
   <li><a href="news.php">Aggiornamenti</a></li>
   <li><a href="mirrors.php">Mirrors</a></li>
   <li><a href="legaldocs.php">Documentazione legale</a></li>
   <li><a href="rassegna.php">Rassegna stampa/web</a></li>
   <li>Comunicati:
	<ul>
	<li><a href="comunicato.html">Primo Comunicato A/I</a></li>
	<li><a href="comunicato_ricorso.php">Secondo Comunicato A/I</a></li>
	<li><a href="comunicato_zenmai23.php">Comunicato Zenmai23</a></li>
	</ul></li>
<li><a href="link.php">Link su Trenitalia</a></li>
  </ul>

  <hr/>

  <ul>
   <li><a href="protesta.php">Cosa posso fare?</a></li>
	<ol>
	<li><a href="protesta.php#mail">Manda una mail a Trenitalia</a></li>
	<li><a href="protesta.php#banner">Pubblica il banner sul tuo sito</a></li>
	<li>Firma il <a href="http://www.autistici.org/ai/trenitalia/guest/guestbook.php">Guestbook</a></li>
	<li><a href="protesta.php#signature">Usa questa signature!</a></li>
	<li><a href="protesta.php#stampa">Stampa e diffondi il materiale</a></li>
	<li>Fai una <a href="http://autistici.org/it/donate.html">donazione!</a></li>
	</ol>
  </ul>

  <hr/>

  <a href="http://www.inventati.org">Home Page del progetto Autistici/Inventati</a><br/>

 </div>


 <div id="main">


<h2>Documentazione legale</h2>

<p>
La documentazione legale &egrave; pubblicamente consultabile:
<ol>
  <li><a href="documenti/1_denuncia_trenitalia/">denuncia di Trenitalia</a></li>
  <li><a href="documenti/2_memoria_autistici.txt">memoria di Autistici/Inventati</a></li>
  <li><a href="documenti/3_replica_trenitalia/">replica di Trenitalia</a></li>
  <li><a href="documenti/4_controreplica_autistici.txt">controreplica di A/I</a></li>
  <li><a href="documenti/5_sentenza_ricorso.html">sentenza relativa al ricorso</a></li>
</ol>


  <br/><br/><br/><br/>
  <hr/>
  <p align="center">
   <small><em>
    Pagina modificata il 
    13/01/2007   </em></small>
  </p>


 </div>

</body>
</html>

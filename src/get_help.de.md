title: Keine Panik!
----

Keine Panik!
============

Autistici/Inventati Hilfe Seite
-------------------------------

Auf dieser Seite versuchen wir Ihnen zu zeigen, was Sie tun können, wenn Sie Hilfe benötigen. Wir haben einige Instrumentarien eingerichtet,
um die Anzahl eingehender Support-E-Mails gering zu halten. Bevor Sie uns also schreiben, **bitten wir Sie inständig erst zu prüfen, ob Sie
hier nicht selbst eine Lösung für Ihr Problem finden können**.


- [Cavallette](https://cavallette.noblogs.org/) - News?
- [FAQ](/docs/faq/) - eine Reihe der am häufig gestellten Fragen (samt Antworten!)
- [Anleitungen](/docs/) - eine Sammlung von Anleitungen zum Einrichten verschiedener Clients um unsere Dienste nutzen zu können
- [Helpdesk](http://helpdesk.autistici.org/) - hier können Sie eine möglichst genaue Problembeschreibung hinterlassen und auf eine Antwort warten, die kommen wird, sobald wir Zeit dafür haben (die Angabe einer Kontakt-E-Mail-Adresse ist dafür unerlässlich)

Außerdem stehen die Chancen nicht schlecht, dass Sie jemanden von uns im **\#ai**-Channel auf dem [autistici.org IRC-Server](/docs/irc/) antreffen.
Die Leute dort haben möglicherweise ein paar Ratschläge für Sie parat oder sind bereit, Ihnen bei kleineren technischen Problemen zu helfen.

<a name="gpgkey"></a>

Wenn Sie wirklich verzweifelt sind - aber wir reden hier von einer 'Ich-stehe-kurz-vor-grausamen-Tod'-Verzweiflung -
können Sie uns auch schreiben, möglichst unter Verwendung unseres [PGP-Schlüssels](gpg_key) (verfügbar auf Schlüsselservern weltweit).

title: Niente panico!
----

Niente panico!
==============

Come trovare aiuto su A/I
-------------------------

In questa pagina ti spieghiamo cosa puoi fare quando credi di aver bisogno del nostro aiuto. Abbiamo realizzato alcuni strumenti per cercare
di ridurre il traffico di email. Prima di scriverci, quindi, **ti invitiamo caldamente a cercare qui la soluzione al tuo problema**.

- [Cavallette](https://cavallette.noblogs.org/) - News?
- [Le domande più frequenti](/docs/faq) (con relative risposte)
- [Manuali](/docs/) - I manuali dei nostri servizi.
- [Helpdesk](http://helpdesk.autistici.org/) - qui puoi lasciare una comunicazione specifica riguardo al tuo problema, ti sarà risposto appena possibile (attenzione a lasciare un indirizzo email dove possiamo risponderti!)

In genere è possibile trovare qualcuno di noi anche sul canale IRC **\#ai** sui nostri server
[irc.autistici.org](/docs/irc/). Avendo tempo, qualcuno potrà darvi consigli e informazioni o risolvere
piccoli problemi tecnici.

<a name="gpgkey"></a>

Se siete proprio disperati e proprio non sapete cosa fare, [scriveteci](mailto:info@autistici.org), possibilmente
usando la nostra [chiave GPG](gpg_key) (disponibile su molti keyserver)


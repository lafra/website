title: Mensajería instantánea y Chat
----

Mensajería instantánea y Chat
=============================

La mensajería instantánea y las salas de chat son cada vez más utilizadas en todo el mundo, pero los servicios más extendidos son
comerciales y no ofrecen ninguna medida de privacidad.

La red IRC network y el servicio Jabber de A/I te permite una comunicación libre y al mismo tiempo una mayor protección del anonimato.

<a name="irc"></a>

Irc - Servicio de chat
----------------------

A/I ofrece a sus usuari\*s chat via IRC (Internet Relay Chat), un sistema de chat generalizado.
 IRC es bastante sencillo de utilizar y mucha gente consigue utilizarlo rápidamente: es por esto que se trata de una de las herramientas de
comunicación electrónica más extendidas (más que el e-mail por ahora).

La red de IRC que nosotr\*s ofrecemos es llamada **mufhd0** y es administrada por A/I, y los proyectos [ECN](http://www.ecn.org) y
[indivia](http://indivia.net) conjuntamente. Tu puedes acceder a la red desde cada uno de los nodos de estos proyectos, a través de
un cliente de IRC cómo [X-Chat](/docs/irc/xchat) (para Linux y Windows), o [Irssi](/docs/irc/irssi) (línea de comandos Linux).

De entre todas estas herramientas, recomendamos X-Chat, dado que permite **conexiones encriptadas vía SSL** y también permite conectarse a
IRC a través de [Tor](/docs/anon/tor).

Para conocer cómo configuar el cliente IRC y cuales son los canales disponibles en nuestra red, para más información sobre los comandos y
servicios IRC, consulta nuestros [howtos](/docs/irc/).

<a name="jabber"></a>

Jabber - mensajería instantánea
-------------------------------

Jabber es el sistema de comunicación más rápido para la gente conectada a Internet. Permite chat y la mensajería instantánea (como MSN,
Google Talk, Yahoo IM, y alguna otra), pero actualmente es mucho más flexible.

Admás Jabber suporta de forma nativa conexiones SSL y en algunos casos puede ser configurado para gestionar comunicaciones a través de
claves de cifrado administradas directamente por l\*s usuari\*s (GPG u OTR por ejemplo). Nuestra sugerencia es
[Pidgin y su plugin OTR](/docs/jabber/pidgin) cómo cliente para este tipo de uso.

Cada una de nuestras cuentas de correo tiene asociada automáticamente una cuenta de jabber con el mismo usuario *usuario@dominio.org*

Para aprendre cómo configurar un cliente de jabber, información de comandos y uso, al igual que los servicios que ofrece jabber, por favor
revisa nuestro [manual de jabber](/docs/jabber/).



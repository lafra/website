title: Willkommen bei Autistici/Inventati
----

Willkommen bei Autistici/Inventati
==================================

A/I (das steht für autistici.org / inventati.org) wurde im März 2001 von einer Gemeinschaft aus Individuen und Kollektiven gegründet, die
sich mit Technologie, Privatsphäre, digitalen Rechten und Politik beschäftigt. Die Grundidee besteht darin, freie und kostenlose
Kommunikationsmittel im großen Maßstab bereitzustellen, um so die Menschen dazu zu bewegen, sich für freie statt für kommerzielle bzw.
proprietäre Kommunikationsweisen zu entscheiden. Wir möchten aufklären und tätig werden, hinsichtlich der Notwendigkeit, die eigene
Privatsphäre zu schützen und sich der wahllosen Beraubung von Daten und Persönlichkeit zu entziehen, wie sie von Regierungen und Konzernen
betrieben wird.

A/I [bietet](/services/), die
die Privatsphäre wahren, an:

[Blogs](/services/blog "Blog bei Noblogs.org") /
[Web-Hosting](/services/website "Website bei A/I")
[Anonymisierungsdienste](/services/anon "Anonymisierungsdienste bei A/I") / [Persönliche VPNs](https://vpn.autistici.org/ "Persönliches A/I VPN")
[E-Mail-Konten](/services/mail "E-Mail-Konto bei A/I") / [Mailinglistem, Newsletter und Foren](/services/lists "Mailingliste bei A/I")
[Instant-Messaging und Chat](/services/chat "Instant-Messaging und Chat bei A/I")
[Mehr](/services).

Alle beantragten Dienste werden unter der Voraussetzung bereitgestellt, dass unsere [Richtlinien](/who/policy)
respektiert und unser [Manifest](/who/manifesto) geteilt wird.

**[Beantrage einen Dienst](get_service)**

**Regelmäßige Informationen (auf Italienisch und manchmal auf Englisch) finden sich in unserem [Blog](https://cavallette.noblogs.org/).


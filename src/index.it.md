title: Benvenut@ su A/I
----

Benvenut@ su A/I
================

A/I (che sta per autistici.org / inventati.org) nasce nel 2001
dall'incontro di individualità e collettivi provenienti dal mondo
antagonista e anticapitalista, impegnati a lavorare con le tecnologie
e attivi nella lotta per i diritti digitali.

Crediamo che questo non sia affatto il migliore dei mondi
possibili. La nostra risposta è offrire ad attivisti, gruppi e
collettivi piattaforme per una comunicazione più libera e strumenti
digitali per l'autodifesa della privacy, come per esempio
[email](/services/mail), [blog](/services/blog),
[mailing list](/services/lists), [instant messaging](/services/chat) e
[altro](/services).

Per usare i nostri servizi verifica di essere in linea con il nostro
[manifesto](/who/manifesto), impegnati a rispettare la
[policy](/who/policy).

**[Richiedi un servizio](get_service)**

**Segui il nostro [blog](https://cavallette.noblogs.org)**


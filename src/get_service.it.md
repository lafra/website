title: Chiedi un servizio
----

# Chiedi un servizio

Vuoi un'email, una mailing list, un sito con A/I?
 Il motivo è che ci tieni alla tua privacy e vuoi anche un account scollegato dalla tua identità reale?

È un motivo decisamente condivisibile.
I nostri motivi tuttavia si spingono più in là della privacy e dell'anonimato: Autistici/Inventati è un collettivo che fa propri e promuove
l'**anticapitalismo**, l'**antirazzismo**, l'**antifascismo**, l'**antisessismo** e l'**antimilitarismo**, oltre al **rifiuto
dell'autoritarismo e del principio di delega**.
Il lavoro che facciamo è tutto volontario. Se lo facciamo, è perché crediamo fermamente in questi principi, e le persone a cui vogliamo
offrire i nostri servizi sono quelle che li condividono.
Per questo, quando ci spiegherai cosa ti spinge qui nel form di richiesta dei servizi, non dirci che vuoi privacy e/o anonimato -- questo
lo diamo per scontato, sennò non saresti qui.
Raccontaci invece dei motivi per cui condividi i nostri principi e di chi sei. Non occorre scendere nei dettagli della tua vita personale,
però dacci un quadro di perché dovremmo offrire un servizio proprio a te.

Se vuoi sapere di più su di noi, leggi il nostro [manifesto](/who/manifesto), la nostra [policy](/who/policy) e la [privacy policy](/who/privacy-policy).

Ogni richiesta sarà letta da una persona in carne e ossa e non da un robot. Apprezziamo quindi ogni informazione che vorrai condividere con
noi, e speriamo di poter instaurare un rapporto di fiducia, a cominciare dal fatto che anche questa corrispondenza verrà distrutta.
La fiducia però è bidirezionale: così come crediamo a quel che dirai senza richiedere i tuoi dati personali, se vedremo che violi i nostri
principi pubblicamente usando i nostri servizi, non esiteremo a rimuovere il tuo account senza nessun preavviso. Se decidi di usare i nostri
servizi, accertati che questo sia il posto giusto per te.

Riceviamo inoltre molte richieste che hanno come motivazione "evitare il controllo delle multinazionali e dell'NSA".
I nostri servizi hanno caratteristiche che **consentono** un livello di privacy che risponda a questo bisogno, ma come sempre, nessun
problema politico ha una risposta esclusivamente tecnica.
In primo luogo tieni presente che **l'uso che fai dei nostri servizi è l'aspetto più determinante per la tua privacy**: in altre parole,
non aspettarti che usare i nostri servizi sia come premere un pulsante magico che ti rende non rintracciabile.
Se per esempio utilizzerai il tuo account A/I per ottenere una sottoscrizione a servizi che tracciano il tuo indirizzo IP, che sono legati
al tuo numero di telefono personale, o che sistematicamente mappano e registrano le tue interazioni sociali, servirà a poco essere solamente
utente di una mail anonima.

**In ultima analisi, l'unica persona responsabile della tua privacy sei tu**: noi rendiamo semplicemente questo processo tecnicamente
possibile, quando e se il tuo uso degli strumenti lo consente.

Speriamo che tu faccia un uso consapevole della tecnologia e dei mezzi di comunicazione che hai a disposizione, e che a questo si affianchi
la rivendicazione politica, in real life, della tua libertà di espressione e di comunicazione.

Infine ricorda che **i nostri servizi sono gratuiti, ma comportano dei costi per noi**.
Dal momento che per tenere in piedi questi server non basta una semplice manutenzione quotidiana (che viene effettuata su base volontaria e
senza nessun compenso economico), apprezziamo molto tutti i contributi volontari per sostenere le spese.

[Scopri come fare una donazione ad Autistici/Inventati](/donate "donate").

**[Chiedi un servizio](https://services.autistici.org/)**

title: Terms of Service
----

Preface
=======

To be hosted on our servers you have to share our principles of **anti-fascism,
anti-racism, anti-sexism, anti-homophobia, anti-transphobia, and
anti-militarism**. Your projects must as well be based on the same
**non-commercial nature** which keep our project alive, and on the desire to
share and experience relationships and struggles, with all the patience it
requires.

Any service provided on our servers cannot be destined to (directly or
indirectly) commercial or religious activities, nor to political parties: to
make a long story short, we do not host anyone who already has means and
resources to spread widely his/her own contents and ideas, or who uses the
concept of (explicit or implicit) delegation and representation in its
day-to-day relationships and projects.

The servers only keeps the logs that are strictly necessary for debug operations,
and they do not hold any personal data connecting accounts on our servers to
actual identities. To be aware of how we respect your data, remember to read [our privacy policy](/who/privacy-policy.en).

Terms of Service
================

April 13th , 2020

Please, read this document carefully.

These Terms of Service (“ToS” or “Terms”) constitute a binding agreement between You 
(the User) and the association A/I-ODV (“A/I” or the generic subject “we”). 
Through the access to our platform and the use of our Services, you signify your 
acceptance of these ToS. A/I Services include - but are not limited to - emails, 
websites, blogs, mailing lists, newsletters, irc, jabber, anonymous remailer and 
the nym server. If you are acting on behalf of an organization, you are acknowledging 
that you have the authority to commit to these Terms for the organization. If you do 
not comply with this agreement, you are prohibited from using our services.

A/I is a non-profit association sharing technical knowledge and promoting social 
awareness about information systems. 

Our project is exclusively run by volunteers, who do not receive any form of payment 
nor compensation for their efforts and expenses. A/I sources money for its project 
through spontaneous donation-based crowdfunding in a nontransactional fashion. 
Our work aims to enact the principles of solidarity and self-organization.

Be sure to read our [Privacy Policy](/who/privacy-policy.en) too, regarding the way we collect and process user 
data and information, as it forms an integral part of these Terms of Services.

We encourage you to reach us at [associazione@ai-odv.org](mailto:associazione@ai-odv.org) if you have any questions or 
concerns about these Terms, or to let us know about any violation perpetrated by A/I users.

### Service request

In order to access our Services it is of utmost importance to read and understand 
our [Manifesto](/who/manifesto) and policies, as they illustrate every relevant aspect of our mission. 
Subjects willing to create an A/I user shall consider whether they agree with the 
principles behind the project or not, before starting the request process.

When requesting an account would-be users are asked to explain, in written form, 
how the objectives they intend to pursue using our platform align with our principles, 
and are otherwise discouraged from using it when this is not the case. 

A/I merely selects and filters the individuals to whom the Services are delivered to
in order to reduce the risk of violations of our policies. By preventively asking 
to submit a written statement of intents, we can evaluate the affinity of every 
Service request from potential new users before any account is approved; 
the aforementioned statements are always evaluated in person by an appointee of A/I 
and deleted right after the request process is terminated. 

Every written statement always undertakes a non-automated process of case-by-case 
examination, occasionally involving some follow-up questions and observations that the 
appointee will send to the requestor in dialogue via a dedicated webpage. At the end of 
the process the request is declared compatible with A/I principles or not, resulting 
in a new account creation or in a rejection of the account request accordingly.
Account requests and all related messages are always deleted right after the request 
approval process is terminated. We highly discourage requestors from providing 
false information during this process, as accounts will be suspended and erased without 
notice every time we will recognize a non principle-compliant behavior. 

### Users of the Services

Pursuant to the GDPR and to the Italian Privacy Law, you must be at least 14 years old to 
have an account for our Services. Services are provided exclusively to humans, therefore 
any account registered by bots or other automated methods is not allowed 
and will be terminated.

### Content Responsibility

We allow our users to upload content online by publicly posting or privately transmitting 
it through our Services, most of which only consist in software automatically providing 
unsupervised, encrypted and non-monitored data conduit. You are the sole responsible for 
the data you store on the platform and the content you make available through our 
Services including its appropriateness, reliability and lawfulness. We do in no way 
pre-screen, filter or verify the content uploaded by the users, nor we have the opportunity 
to edit or modify it.

Every user is responsible for his or her use of the Services and 
any consequences thereof.

Due to the noncommercial nature of our association we can only count on the work of our 
volunteers when they are available, therefore we cannot guarantee the safeness of content 
and materials distributed through the Services at any time. Any violation or complaint 
can be brought to the attention of A/I via email or to our [helpdesk](https://helpdesk.autistici.org/) ticketing system.

### Conduct Regulation and Restrictions

The permission to use A/I Services is subject to the restrictions set forth in these 
Terms. The user is not allowed to misuse any account, our infrastructure or the Services 
for purposes incompatible with our mission and policies, or to assist, encourage or 
enable others in doing so. A/I defines misuse, specifically, as follows:

1. Promoting fascism, nazism, militarism, state nationalism or contributing to the 
discrimination, harassment or harm against any individual or group through forms of 
discrimination based on gender, race, religion or sexual orientation, along with 
issuing statements which are racist, xenophobic or discriminatory in any other form.

2. Sending advertisement or unsolicited messages, spam, junk mail, bulk emails or 
mailing list emails to persons who have not specifically agreed to be their 
recipients, or making the recipient address available as a response address for spam 
sent via a third party, as well as using email for purposes such as “no reply” type 
of accounts for a business-related purpose.

3. Publishing of others’ personal information, whenever not already publicly 
available, especially when it is done by violating the personal sphere of an 
individual or does not constitute a legitimate right of reporting such information.

4. Tampering with the configuration, expected behaviour or proper operation of 
the Services in any manner that could damage, disable, overburden or impair 
their functioning or that could disrupt, negatively affect or inhibit other users 
from fully benefitting from the exact functionality the Services were originally 
designed to provide, as well as intercepting any content or information that we 
have not intentionally made available. 

5. Bypassing or invalidating any access control that we have 
implemented or attempting to “scrape”, "exfiltrate" or “harvest” content.

6. Disclosing, selling or purchasing the access code to use our Services, and trading or 
delivering any of A/I Services to a third party.

7. Using the Services for financial gain, including but not limited to trading and 
managing sales, commercial projects, enterprises and other profit oriented 
activities, (exceptions can be made for those project that put DIY or mutual 
support and local solidarity before the concept of capital and personal income, 
when this is well documented and discussed with us when requesting the service).

8. Using the Services for cryptocurrency related activities;

9. Using the Services in order to promote institutional political parties or any 
other organization that already has the financial resources to widely spread its 
own content and ideas, or those who use the concept of (explicit or implicit) 
delegation and representation in its day-to-day relationships and projects.

10. Using the Services for any military purpose, including information or training 
material about firearms and related combat techniques, cyberwarfare, weapons 
development and manufacture.

11. Otherwise infringing or circumventing these Terms.

Any kind of misuse or abuse may result in account deletion without further notice.

Any subject found violating these restrictions shall be obliged to indemnify A/I 
against all third party claims originated from his or her behavior, including 
compensation for the expenses incurred by the association in order to conduct any 
appropriate fact finding and obtain legal defense. Frivolous or vexatious 
complaints shall give us right to compensation too, as they impose an unreasonable 
burden and a waste of time and resources on us.

Moreover, your access and use of A/I Services is subject to these Terms and all 
applicable laws and regulations. We will promptly inform you if any legal 
issue should arise. 

### Service Specific Policies


#### **Mailboxes**

There are no backups of your data, therefore you should regularly download your mail 
to your local machine using an email client. Our efforts are aimed only at ensuring 
the continuity of mail service (send/receive). Even if we do not enforce disk quotas, 
users shall respect the reasonable storage limits in any way possible; for this reason, 
we invite you to limit the use you make of our disks by downloading or deleting your 
emails as often as possible. 

To use your email account, you will have to choose a simple recovery question which will allow you 
to recover access to your mailbox in case you lose/forget your password. If you also 
forget the question or the answer to the question we will not be able to re-send you 
the data to access your account again. The only thing we will be able to do is creating 
a new mail account for you, but with no chance of recovering your old data. 

Consequentially users shall remember to set the “recovery question” in their account by 
clicking on the link they find in their user panel. When you create an account you also
agree to maintain the security of your password and accept all risks of unauthorized 
access to your account data.

#### **Websites and Blogs**

We provide web space only for static contents. Alternatively, you shall use our blogging 
platform, [Noblogs](https://noblogs.org).

Server space left unused for more than 120 days from the activation will be disabled 
and deleted. Space resources are limited, therefore if you need to upload a large amount 
of data you should contact us beforehand.

We provide a web statistic tool as part of our Services, therefore any other tool with 
the same purpose is forbidden. 

It is not allowed to use the assigned space as a re-direct for other sites. 

Responsibility for the content of the sites lies within the sites’ webmasters. Webmaster 
are legally liable for their web content. Whenever a legal notice from an official source 
reaches us, we reserve the right to conduct an appropriate fact finding procedure that may also possibly lead to 
making the page unaccessibile; however we can consider to discuss the issue with those 
who are directly involved, in order to find the most suitable solutions for our users 
as well as pledging to our mission and policies. 

### Copyright and Intellectual Property infringement

In case we are notified of any intellectual property violation occurred on our platform, 
whenever the interested party will provide us with sufficient and specific information 
necessary to identify and locate the allegedly infringing materials, we will remove 
from our platform any content that may infringe or violate patent, trademark, trade 
secret, copyright, right of publicity, intellectual rights or other similar rights of 
any third party. Upon review, A/I may decline to remove allegedly infringing 
materials – for example, if we determine they are not infringing, if we lack adequate 
information to determine that they are infringing, if we are unable to find the accused 
materials, or if the materials are protected by the fair use doctrine, among other 
reasons. If you are a copyright owner and you believe that some specific content 
on A/I violates your rights, please contact us by emailing 
[associazione@ai-odv.org](mailto:associazione@ai-odv.org) describing the issue.

### Account Termination

If you breach or fail to comply with any of these Terms, A/I has the right to suspend or 
disable your access to the Services, without notice. Any kind of abuse may result 
in immediate account termination. In the case of a period of inactivity greater than or 
equal to 12 months, we reserve the right to delete your A/I account and all related data.

### Limitation on Liability

You agree that A/I (AI-ODV) is not liable to you or to any other party for any direct, 
indirect, coincidental, specific, consequential or exemplary damages, including but not 
limited to: damages due to business interruption, loss of profits, goodwill, use, data, 
or other intangible losses arising out of or in connection with the use or inability to 
use our Services, regardless of whether we were notified verbally or in written form of the 
possibility of such damage. In the case that applicable law may not allow the limitation 
or exclusion of liability or incidental or consequential damages, the above limitation or 
exclusion may not apply to you, although our liability will be limited to the fullest 
extent permitted by applicable law. 

If you have a dispute with one or more of our users, you agree to release A/I from any 
and all claims, demands and damages (actual and consequential) of every kind and nature, 
known and unknown, arising out of or in any way connected with such disputes.

It is the users responsibility to keep their password safe and secret and to prevent 
accounts from being accessed by a third party. If, however, a third party should 
unauthorizedly gain access to the user’s account and perform any misuse of the Services, 
only the registered user of the account can and will be held liable. 

Furthermore, you will not hold A/I liable or seek indemnification if confidential material 
is unintentionally released as the result of a security failure or vulnerability in the 
performance of the Services.

We will have no liability for any failure or delay due to matters beyond our 
reasonable control.

### Disclaimer of Warranties

Regardless of our best efforts, you understand and agree that A/I offers you internet-based 
services, which are consequently subject to availability limitations without liability 
in your regards, “as is” and without any warranty, express, implied, or statutory. We specifically 
disclaim any implied warranties of title, merchantability, fitness for a particularly 
purpose and non-infringement. We provide no warranty regarding the completeness, accessibility, 
accuracy, adequacy, suitability, functionality or quality of our Services.

A/I does not guarantee that the Services will meet your requirements or that will be 
uninterrupted, timely, secure, or error-free, nor that the information provided is 
accurate, reliable or correct and that any defects or errors will be corrected. 
A/I cannot ensure that the Services will be available at any particular time or location.

You acknowledge that we do not have control over your use of the Services and we do not 
guarantee the performance or results that may be obtained through your use of them, 
neither do we guarantee or claim that your use of the Services will not infringe 
the rights of any third parties.

You agree that the use of our Services is at your sole and exclusive risk. 

### Governing Law

These Terms of Service and your use of A/I Services are governed in all respects by the 
substantive and procedural laws of Italy, without regard to conflict of law provisions 
except to the extent applicable law provides otherwise. Any right, obligation and relation 
of the parties under these Terms will be construed and determined in accordance with the 
Italian law. You irrevocably submit to the exclusive jurisdiction and venue of the courts 
of the Italian State over any and all disputes arising out of or relating in any way to 
these Terms or A/I Services. You agree that the English language version of our 
Terms of Service is controlling in any claim or dispute.

### Changes to this Terms of Services

We reserve the right to change, modify or update these Terms at our sole discretion at any 
time, for any or no reason, without liability. If we make major changes, we will notify our 
users in a clear and direct manner. Minor changes may only be highlighted in the footer 
of our website.

These Terms constitute the entire agreement between you and A/I regarding the use of the 
Services, suspending any prior arrangement. You are responsible for regularly reviewing 
these Terms of Services, therefore continued use of the Service after such changes shall 
constitute your consent to them. If you do not accept the changes, your only and exclusive 
possibility is to discontinue using the Services.

### Miscellaneous

Throughout this Agreement, each section includes titles. These section titles are not 
legally binding.

If you discover or suspect the existance of a breach in the security of Services, 
please let us know as soon as possible. You will promptly notify A/I if you become aware 
of any unauthorized use of, or access to, our Services through your account, including 
any unauthorized use of your password or account.

A/I only [offers support](https://helpdesk.autistici.org/) via email, in-Service communications, and electronic messages. 
We do not offer telephone support.

Communications made through email will not constitute legal notice to A/I or any of its 
volunteers in any situation where notifying us is required by contract or any law or 
regulation. Legal notice to A/I must be in writing and served in a language understandable to us.

For further insights as to our mission and ideals, please read our [Manifesto](manifesto).

(This is the original version of this policy. This document has also been translated in: [IT](/who/policy.it) )

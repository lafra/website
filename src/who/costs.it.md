title: I costi dell'infrastruttura
----

I costi dell'infrastruttura
===========================

Abbiamo pensato che sia giusto che tutti coloro i quali contribuiscono ogni anno con le loro donazioni a mantenere l'infrastruttura di A/I
sappiano quali sono i costi che sosteniamo ogni mese.

In sostanza i costi possono essere divisi in **costi di connettività** per ogni macchina della rete di A/I e in **costi di manutenzione**
(che coprono hardware che si sfascia, server nuovi da acquistare, in rarissimi casi la copertura delle spese di viaggio di chi si reca a
sistemare i vari disastri che regolarmente avvengono :).

**Connettività (cifra annuale)**

circa 8300 euro

**Costi bancari (cifra annuale)**

circa 600 euro

**Amministrazione (cifra annuale)** \[registrazione domini, contabilità ecc.\]

circa 1540 euro

**Manutenzione (cifra annuale)** \[hardware, server, costi di viaggio, spese legali, emergenze\]

circa 3300 euro

**TOTALE circa 13740 euro all'anno**

Ciò significa che ogni anno l'infrastruttura di autistici costa più o meno 14.000 euro.

Tutto il lavoro del collettivo per mantenere la struttura è svolto a titolo gratuito. **Sopravviviamo grazie alle donazioni di chi usufruisce dei nostri servizi**.

Quindi fai una [donazione](/donate) ora!

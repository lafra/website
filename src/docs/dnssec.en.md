title: DNSSEC
----

DNSSEC - what is it and how it works
====================================

Every time a device connected to the internet wants to communicate with
another device (e.g. your computer and A/I servers) it must do so by
contacting said device on its IP address. It would be unconvenient to do
all of that manually and remember IP addresses, thus the DNS has been
invented to translate names (e.g. www.autistici.org) into IP addresses
(e.g. 82.221.99.153).

DNS itself doesn't provide any guarantee over authenticity of its
replies. This means that anybody controlling your traffic is capable of
changing the IP addresses your computer sees. This scenario is exploited
for example for very simple censorship: if the site is censored then
your ISP DNS servers will return no IP addresses instead of the real
ones.

The DNSSEC protocol has been introduced to provide an additional layer
of security by making sure that replies can be authenticated. DNSSEC is
not perfect (see for example the problem of [*zone
enumeration*](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions#Zone_enumeration_issue.2C_controversy.2C_and_NSEC3)
and [criticism by D. J. Bernstein](http://cr.yp.to/djbdns/forgery.html))
however we think that it is important to provide users and additional
validated channel.

How do I use it?
----------------

One of the simplest ways to use DNSSEC is to use a public
DNSSEC-validating DNS (such as google public dns). However this solution
assumes that the communication between your device and the DNS server is
to be trusted.

Instead, you can run locally a validating DNS server that will do the
validation. There are pre-packaged solutions to ease this task such as
[dnssec-trigger](https://www.nlnetlabs.nl/projects/dnssec-trigger/)
oriented to installation on roaming laptops.

Is it working?
--------------

You can verify that DNSSEC is working properly by assessing whether it
is working or **not**. Specifically, there are websites that are broken
*on purpose* (e.g. [dnssec-failed.org](http://www.dnssec-failed.org))
and your device won't be able to use them. In particular your device
won't be able to know the website's IP address. The command below can be
used to verify using the terminal (if for example you are using a local
resolver as explained above). Notice the command resuts no results
(status: SERVFAIL):

``` {.code}
$ dig  +dnssec +noauth +noquestion +nocmd +nostats dnssec-failed.org @localhost
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: SERVFAIL, id: 49462
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags: do; udp: 1024
```

On a valid domain however be the valid answer flag "ad" will be set by
the local resolver:

``` {.code}
$ dig  +dnssec +noauth +noquestion +nocmd +nostats autistici.org @localhost
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 63802
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 4, AUTHORITY: 6, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags: do; udp: 1024
;; ANSWER SECTION:
autistici.org.                9382        IN        A        82.221.99.153
autistici.org.                9382        IN        A        178.255.144.35
autistici.org.                9382        IN        A        82.94.249.234
autistici.org.                9382        IN        RRSIG        A 7 2 9600 20140110201142 20131211201142 24242 autistici.org. VMeyEaa3iiCwlDwhch6erY0ScJkdli9P+xzoRP27Ww7/hGea3rwo4hpX 3M8HFwDIkE3aQQLDwvSvlFcB0XzvKdrbOgKXRW4nvaAvhEO5E8eKYmgb oMphm2RoU1+I6r+WWpKpaE/o6bOzDgZ43j1KkjOuEODA/3vbN7G7gIg8 JDw=
```

How to validate using the command line?
---------------------------------------

If you are using a local validating DNS, all answers directed to that
server will be already validated. In cases where this isn't possible you
can still manually validate a request (in a slighly convoluted way):

The first step is to obtain the root "key", e.g.

``` {.code}
dig +nocomments +nostats +nocmd +noquestion -t dnskey . > trusted-root.key
```

then use this key as a *trusted* key used to validate answers:

``` {.code}
dig +sigchase +trusted-key=trusted-root.key www.autistici.org A
```

Other uses
----------

The hierarchial nature of DNS is also reflected in the DNSSEC
deployment, this means that each entity in the DNS tree can autonomously
publish and generate its keys. Compare this to the existing CA
infrastracture for https certificates with a few valid entities that are
globally trusted. With DNSSEC it is possible to freely change the keys
by simply publish them to your parent zone. (e.g. autistici.org
publishes its keys towards org.)

DNS supports other data types other than IP addresses, e.g. you could
publish SSL certificate information for a given domain. The
[DANE](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities)
protocol aims at specifying the details of TLS certificate fingerprints
over DNS and bypassing the root CA system altogether. 


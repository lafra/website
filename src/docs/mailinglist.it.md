title: Come si usano le mailing list
----

Come si usano le mailing list
==================

Che cos'è una mailing list - Mailing list pubbliche e private
-------------------------------------------------------------

Una mailing list è sostanzialmente una lista di indirizzi a cui automaticamente vengono inviati dei messaggi da un programma presente sul
server (la macchina che fornisce il servizio).

A seconda di come viene gestita la mailing list, vi possono scrivere soltanto gli iscritti (lista cosiddetta **chiusa**) o anche altri
(lista **aperta**), mentre in ogni caso i messaggi saranno ricevuti dai soli iscritti.

Una mailing list può essere **pubblica** o **riservata**, a seconda che i messaggi che transitano per essa siano anche pubblicati su web
(come nel nostro [archivio pubblico](http://lists.autistici.org)e newsgroup o meno.

Tenete sempre presente che se una lista è pubblica, **i messaggi che scrivete presto o tardi compariranno su Google e altri motori di
ricerca**, è importante avere sempre questa consapevolezza quando si scrive.

Per partecipare e ricevere i messaggi di una certa mailing list è di solito necessario iscriversi, ma per le liste **aperte e pubbliche** è
possibile scrivere senza essere iscritti e leggere la mailing list via web o newsgroup.

Per iscriversi basta recarsi sull'[elenco](/services/hosted/lists) delle liste **pubbliche** presenti sui server di a/i
e con un paio di click arrivare alla pagina dedicata alla lista che vi interessa; completando un semplice form vi verrà recapitata nella
casella di posta una richiesta di conferma (onde evitare che qualcuno iscriva indirizzi non propri) e una volta risposto ad essa comincerete
a ricevere i messaggi di quella lista.

Le mailing list sono degli ottimi strumenti di comunicazione, di coordinamento su progetti che si svolgono con la partecipazione di soggetti
anche molto distanti tra loro, di approfondimento di alcune tematiche, se usate correttamente. Di fatto non esistono regole generali, ed
ogni lista ha i suoi usi e costumi che fanno parte della storia della comunità di quella lista.

Enjoy!

Amministrare una mailing list
-----------------------------

Quando aprite una mailing list sui nostri server la prima cosa che dovrete fare è collegarvi al pannello di amministrazione e configurare le
opzioni della vostra lista: noi abbiamo impostato alcuni valori predefiniti, ma ovviamente questi sono tarati sull'uso che noi *pensiamo* si
faccia generalmente di una lista. I valori predefiniti potrebbero essere **molto distanti** dalle vostre necessità.
 Per cui collegatevi alla pagina **https://www.autistici.org/mailman/admin/NOMELISTA** e cominciate a spulciare le opzioni.

### Opzioni Generali

Nella sezione *Opzioni generali* troverete le impostazioni che riguardano la pagina di benvenuto della lista, la sua descrizione, i messaggi
di benvenuto da inviare agli utenti, la grandezza massima degli allegati che possono inviare gli iscritti, ecc.

La maggior parte dei parametri che trovate predefiniti in questa sezione dovrebbero andare bene (ovviamente le descrizioni e i messaggi di
benvenuto sono a vostra totale discrezione e non hanno valore preimpostato), ma **non dovrete mai cambiare l'opzione "Nome host
(host\_name)"** altrimenti la vostra lista smetterà di funzionare.

### Password

Questa sezione serve ovviamente a reimpostare la password di amministrazione e la password che volete passare ad eventuali persone che
individuerete come moderatori (quindi in grado di approvare e respingere mail di non iscritti alla lista, ma non in grado di modificare
altre opzioni della lista).

### Gestioni iscritti

Non c'è bisogno di dirvi che questa sarà una delle sezioni delle opzioni di impostazione che frequenterete con maggiore assiduità.
 Nell'*Elenco iscritti* troverete un elenco (appunto) di tutte le persone iscritte alla lista seguite da una serie di opzioni:

- *Moderato* significa che ogni messaggio dell'iscritto verrà vagliato dall'amministratore prima di essere inoltrato o meno agli iscritti
- *Nomail* significa che l'iscritto - per diversi motivi - non sta ricevendo le mail della lista (la sua mailbox potrebbe essere
    disabilitata, oppure potrebbe aver deciso di iscriversi solo per inviare mail ma senza riceverne)
- *Digest* e *Testo* indicano con quale modalità l'utente riceve i messaggi della lista: messaggio per messaggio (*Testo*) o in una sola
    mail in cui vengono raccolti i messaggi di un giorno, di una settimana o di un mese (*Digest*)

Se volete disiscrivere un singolo utente, cliccate sul box *Cancella* a fianco della sua mail e poi su *"Applica le modifiche"* in fondo
alla pagina.

Per iscrivere invece molti indirizzi tutti insieme potete visitare la sezione *Iscrizione di massa* dove potrete includere - uno per riga -
un elenco di mail da aggiungere alla lista e un eventuale messaggio di benvenuto personalizzato.

### Opzioni per la privacy

Questa è **una delle sezioni più importanti**. Cominciamo con le *Regole di iscrizione*.

- *Advertised* è l'opzione che indica ai nostri server se la lista deve essere inserita nell'elenco delle liste disponibili sui nostri
    server: ciò significa che l'esistenza della lista sarà **nota al pubblico** inclusa la sua descrizione. Il valore predefinito è **no** e
    così dovrebbe restare se la lista è uno strumento di dibattito interno a un gruppo, collettivo, e via dicendo.
- *Policy di iscrizione* è l'opzione che specifica quali passaggi un utente deve compiere per iscriversi, con complicazioni crescenti: se
    basta rispondere a una mail automatica mandata dal server per essere iscritti; se è necessario attendere l'approvazione
    dell'amministratore prima di essere iscritti; se sono previsti entrambi i passaggi. Il valore predefinito **a partire da aprile 2012** è
    che sono necessari entrambi i passaggi. Precedentemente era sufficiente rispondere a una mail del server, ergo...
- *Policy di disicrizione* è l'opzione che indica se per disiscriversi è necessaria l'approvazione del moderatore. Viviamo in un mondo
    libero quindi il valore predefinito è **no**
- *Banlist* è l'elenco delle mail a cui è vietato iscriversi alla lista
- L'ultima opzione interessante è quella che definisce a chi è consentito vedere gli iscritti alla lista: normalmente l'operazione è
    consentita ai soli iscritti, ma potete decidere di rendere pubblico l'elenco oppure di permettere al solo amministratore
    questa sbirciata.

Nei *Filtri sul mittente* è possibile decidere se i messaggi dei non iscritti debbano essere *respinti*, *trattenuti in moderazione* o
semplicemente *scartati* (ovvero cestinati senza che la cosa venga notificata agli autori).
 E' possibile anche specificare una serie di indirizzi i cui messaggi (pur non essendo questi iscritti alla lista) sono automaticamente
*accettati* e inoltrati agli iscritti, oppure automaticamente *respinti*, *trattenuti* o *scartati*.
 E' modificando i parametri di questa sezione che l'amministratore può creare una lista **aperta** in cui chiunque può scrivere senza che
sia necessaria l'iscrizione alla lista o l'approvazione del moderatore.

### Opzioni di archiviazione

Questa sezione delle opzioni di configurazione della lista **molto importante** è quella in cui l'amministratore decide se una lista avrà
archivi **pubblici** o **privati**. Vi preghiamo di prestare attenzione perché una **lista pubblica** implica che i messaggi degli iscritti
e i loro dati vengano indicizzati dai motori di ricerca!

- *Archivi* definisce se la lista conservi sui nostri server i messaggi inviati o meno. Il valore è predefinito è **no** e quindi i
    messaggi delle vostre liste *non* saranno conservati se non nelle mailbox degli iscritti
- *Pubblici o Privati* è l'opzione che definisce se gli archivi (ove presenti) della lista saranno a disposizione di tutto il pubblico sui
    [nostri server](http://lists.autistici.org/) o solo in una pagina dedicata (il cui link trovate nel pannello di amministrazione
    della lista).

### Controllo delle richieste amministrative pendenti

In questa sezione sono presenti i messaggi trattenuti in moderazione e che attendono una decisione da parte dell'amministratore. Per ogni
messaggio potrete scegliere che cosa fare e se applicare tale scelta a tutti i restanti messaggi dello stesso autore. Le azioni possibili da
parte dell'amministratore sono:

- *Rimanda la decisione*: significa che al momento non verrà presa nessuna decisione in merito al messaggio
- *Accetta*: inoltra il messaggio a tutti gli iscritti
- *Rigetta*: cestina il messaggio e invia una notifica all'autore dell'avvenuto rifiuto
- *Scarta*: cestina il messaggio senza inviare una notifica all'autore



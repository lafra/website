title: Come configurare Xabber per Jabber
----

Come configurare Xabber per Jabber
========================================================

Prendendo ad esempio l'indirizzo user@domain.org:

    User: user
    Server: domain.org
    Check the box 'Custom host'.
    Host: jabber.autistici.org
    Port: 5222
    TLS/SSL: Require TLS

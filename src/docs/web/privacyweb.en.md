title: Some notes about users' privacy
----

Some notes about users' privacy
===============================

A/I do their best not to keep data in their servers which could help identify people surfing their sites. It is thus important for
webmasters to pay attention to the functions of the software they choose to install: many visit counters and many CMSs record for instance
visitors' IPs (independently from the global settings of the A/I servers), and it is crucial to check that these functions are disabled on
your sites to ensure our aim are fulfilled.

We would like to remind you that our servers offer many different services and are to date an important communication tool for thousands of
activists. We hope that being aware about this helps reduce the troubles which may arise due to a careless use of your web space.

Last but not least, our servers only keep the logs strictly necessary for debugging operations.

title: Banners
----

Banners
=======

Hosted by
---------

Da pubblicare sul vostro sito ospitato da A/I

- ![](/static/img/banners/hostedby/icon-hostedby.png) ![](/static/img/banners/hostedby/icon-noblogs.org.png)
- ![](/static/img/banners/hostedby/icon-webcounter.png)

Per esempio potreste copia/incollare il seguente codice html nella vostra homepage:

 &lt;!-- LOGO Hosted by AUTISTICI-INVENTATI --&gt;
 &lt;a href="http://www.inventati.org" target="_blank" rel="noopener"&gt;&lt;img src="http://www.inventati.org/static/img/banners/hostedby/icon-hostedby.png"&gt;&lt;/a&gt;
 &lt;!-- LOGO AUTISTICI-INVENTATI - END --&gt;

- - -

Vecchi banner storici
---------------------

- ![](/static/img/banners/hostedby/host_bianco.png) ![](/static/img/banners/hostedby/host_blue.png)
- ![](/static/img/banners/hostedby/host_nero.png)

### La campagna +kaos

- ![](/static/img/banners/hostedby/125x125kaos.gif)
- ![](/static/img/banners/hostedby/310x60kaos.gif)

### Vecchi banner per siti hosted by

- ![](/static/img/banners/hostedby/ai_bianco.png)
- ![](/static/img/banners/hostedby/ai_nero.png)
- ![](/static/img/banners/hostedby/autistici_button.png)
- ![](/static/img/banners/hostedby/inventati_button.png)
- ![](/static/img/banners/hostedby/banner_sparIAmo.gif)

- - -

Varie Campagne
--------------

- ![](/static/img/banners/campagnaR_120x120.gif)
- ![](/static/img/banners/campagnaR_468x60.gif)
- ![](/static/img/banners/pianoR_120x120.gif)
- ![](/static/img/banners/pianoR_468x60.gif)
- ![](/static/img/banners/NoBlogs_120x120.gif)

- - -

Altri banner
------------

Vedete anche la nostra sezione propaganda: <http://www.inventati.org/propaganda/>

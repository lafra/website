title: Navigazione anonima con Tor 
----

Navigazione anonima con Tor
===========================

Uno degli strumenti che più ci sembrano raccomandabili per tutelare il proprio
anonimato in rete è [Tor](https://www.torproject.org), una rete di tunnel
virtuali che permette di navigare in Internet senza rivelare la propria
identità.

Per navigare in modo anonimo utilizzando Tor, dovete installarvi [Tor
Browser](https://www.torproject.org/projects/torbrowser.html.en) (disponibile
per tutti i sistemi operativi), un browser già configurato per la navigazione
anonima.

Tutto quello che vi serve sapere è sul [sito ufficiale di
Tor](https://torproject.org/documentation.html)

Considerate, comunque, che Tor da solo non è garanzia di anonimato: ci sono dei
rischi di cui essere consapevoli e degli accorgimenti da prendere, tutti
illustrati [qui](https://www.torproject.org/download/download.html.en#Warning).

Accesso ad A/I tramite Hidden Service Tor
-----------------------------------------

I servizi essenziali di A/I sono disponibili anche su [Hidden Service
Tor](https://www.torproject.org/docs/hidden-services.html), all'indirizzo:
[autinv5q6en4gpf4.onion](http://autinv5q6en4gpf4.onion/)

Attualmente i servizi offerti via HS includono il sito principale (incluso
WebADV e webmail), la posta (SMTP e POP/IMAP), Jabber e IRC, tutti disponibili
sullo stesso indirizzo.

Siccome gli indirizzi .onion non sono particolarmente mnemonici, è anche
possibile recuperare questo indirizzo con una query DNS:

    $ dig  +dnssec +noauth +noquestion +nocmd +nostats onion.autistici.org TXT
    @8.8.8.8 ;; Got answer: ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id:
    49449 ;; flags: qr rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL:
    1

    ;; OPT PSEUDOSECTION: ; EDNS: version: 0, flags: do; udp: 512 ;; ANSWER
    SECTION: onion.autistici.org.    9600    IN  TXT "autinv5q6en4gpf4.onion"
    onion.autistici.org.    9600    IN  RRSIG   TXT 7 3 9600 20140110201148
    20131211201148 24242 autistici.org.
    jshkdJJbHpxE0AzHcbQnr2mk75I/qawzLbObVX2A7lw79Sa5UEIjzHfl
    75Vchn0095k9KTJqW2Y9yImxMTDuu3yXP1rmTzd9UXpEA7YFyPP5yOjU
    YUPS9BdzVOzYK9RsZSAOPom5fziDLzatcruI+/bPILbOOgR9vim/pZKr 0XI=

La cosa da notare è che la flag di query validata "ad" è stata settata.
L'esempio sopra usa google public dns ma raccomandiamo di usare un server DNS
locale per validare le risposte come spiegato [qui](/docs/dnssec).  Nota: capire
a quale indirizzo .onion bisogna connettersi è la parte delicata dell'utilizzo
degli Hidden Services con Tor, si consiglia di accedere a questa pagina con SSL
e ricopiarsi l'indirizzo qui sopra in un file locale o altro posto sicuro.


Se invece preferite i vecchi proxy, qui potete trovare delle liste di proxy
pubblici:

- <http://www.freeproxy.ru/en/free_proxy/>
- <http://www.samair.ru/proxy/>



title: IRC Chat Howto
----

IRC Chat Howto
==========

Per poter chattare iniziate a provare (senza impegno ;)) l'interfaccia web di <!-- <a href="http://irc.autistici.org/">A/I</a> o-->
[indivia](https://webirc.indivia.net/cgiirc/irc.cgi) <!--(che offre anche un'opzione di <a
href="https://irc.indivia.net/eirc/index.html">chat crittata</a>)-->. Se poi trovate IRC interessante, potete provare qualche client un po'
più evoluto come [Xchat](/docs/irc/xchat) o [Pidgin](/docs/irc/pidgin).

I dati principali da conoscere per chattare sono:

-   [i parametri di configurazione, vedi sotto](#parm_irc);
-   il *canale* tematico in cui volete chattare (i nomi dei canali iniziano con il simbolo \#, per es. \#indymedia o \#fumetti). Potete
    mettere quello che vi pare. Il canale più frequentato su irc.autistici.org è \#hackit99, ma la rete ospita anche il canale di indymedia
    italia, \#indymedia. Per trovare i membri del Collettivo e fare tutte le domande a cui non risponderemo mai, potete visitare il
    canale \#ai.
-   il *nick* che volete usare, cioè il nome (che può essere il vostro vero nome o un soprannome che vi piace) con cui comparirete in chat.
    Per essere sicur<span class="red">\*</span> che il vostro nick non venga utilizzato da altr<span class="red">\*</span>, **registratelo**
    (per questo e altri servizi, v. il [manuale sui servizi IRC](/docs/irc/irc_services).

I comandi principali che vi serviranno in IRC, e che nei client grafici come [xchat](/docs/irc/xchat) sono sintetizzati in menu
a tendina o altri elementi grafici, sono:

-   /nick seguito dal nick che volete usare per cambiare nick;
-   /join seguito dal nome del canale per entrare in un canale (es. /join \#hackit99);
-   /part seguito dal nome del canale per lasciare un canale;
-   /quit per lasciare IRC;
-   /msg seguito dal nick di qualcun altro per comunicare privatamente con lui (cosidetto private message o query);

Ovviamente ci sono molti altri comandi, per i quali però vi invitiamo a leggere uno dei tanti elenchi di comandi per IRC disponibili online.

Il server IRC di A/I offre alcuni servizi aggiuntivi che vanno sotto il nome di IRC Services. Per saperne di più, leggete la nostra
[guida](/docs/irc/irc_services).

<a name="parm_irc"></a>
Info Tecniche
-------------

- Indirizzo del server: `irc.autistici.org`
- Porta (con SSL): `6697` o `9999`

 Se state usando la rete Tor il server IRC è raggiungibile a questo hidden service: autinv5q6en4gpf4.onion
 Per configurare i diversi client, consultate le apposite guide su [X-Chat](/docs/irc/xchat),
[Pidgin](/docs/irc/pidgin) (Linux e Windows), [Irssi](/docs/irc/irssi) (Linux
command line). Poiché la rete mufhd0 è gestita assieme a [ECN](http://www.ecn.org) e a [Indivia](http://www.indivia.net),
in caso di down dei server di A/I potete collegarvi attraverso questi altri due server.

Contatti Gestione IRC
---------------------

Per qualsiasi problema o richiesta riguardante il servizio IRC e/o la possiblità di link con altri server, scrivete a <irc@autistici.org>.



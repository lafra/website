title: Come configurare XChat 
----

Come configurare XChat
======================

Scaricate XChat da [qui](http://www.xchat.org/download/) o, se usate Windows, da [qui](http://www.silverex.org/download/).

Aprite XChat e nel menu X-Chat cliccate su Lista dei server.

![](/static/img/man_irc/it/xchat1.png)

Vi si aprirà una finestra con un elenco di server. Cliccate su Add e al posto di New Server scrivete un nome che identifichi la nuova rete,
ad esempio mufhd0.

![](/static/img/man_irc/it/xchat2.png)

A questo punto cliccate su Modifica: vi si aprirà una nuova finestra in cui inserire i parametri di configurazione della rete IRC. Cliccate
sul tasto Modifica situato accanto al riquadro Server e al posto di Newserver/6667 scrivete ai.irc.mufhd0.net/9999.

Inserite un segno di spunta nelle caselle Utilizza SSL per tutti i server di questo network e chiudete la finestra.

![](/static/img/man_irc/it/xchat3.png)

A questo punto tornerete alla Lista dei server e non dovrete fare altro che cliccare su Connetti ed entrare nel canale desiderato (v. lista
dei comandi).

<a name="TOR"></a>

Se preferite collegarvi in modo anonimo tramite Tor, innanzitutto installate Tor e Privoxy seguendo le istruzioni di
[questi manuali](https://www.torproject.org/docs/documentation.html), dopodiché dal menu Impostazioni cliccate su Preferenze e nel menu a
sinistra cliccate su Impostazioni di rete.

![](/static/img/man_irc/it/xchat4.png)

Nella finestra Nome Host scrivete localhost, selezionate la porta 9050 e come Tipo scegliete Socks5. Da questo momento in poi vi
collegherete in modo affidabilmente anonimo. Se volete smettere di collegarvi in modo anonimo, basta che sostituiate il tipo Socks5 con
(Disabilitato).

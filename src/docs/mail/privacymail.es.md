title: Protegiendo tu privacidad: cómo y por qué
----

Protegiendo tu privacidad: cómo y por qué
=========================================

Resumen
------------------

 <a name="s1"></a>[1. Sobre la privacidad](#1)

 <a name="s1.1"></a>[1.1. ¿Por qué debería encriptar mi correo?](#1.1)

 <a name="s1.1.1"></a>[1.1.1. Secreto No-personal: negociaciones, finanzas, justicia](#1.1.1)

 <a name="s1.1.2"></a>[1.1.2. Secretos personales: vida privada y sentimientos](#1.1.2)

 <a name="s2"></a>[2. Todos los riesgos que sufre TU casilla de correos](#2)

 <a name="s2.1"></a>[2.1. La ruta de tus mensajes de correo electrónico](#2.1)

 <a name="s2.1.1"></a>[2.1.1. Las incontables copias de tus mensajes de correo electrónico](#2.1.1)

 <a name="s2.2"></a>[2.2. Leyendo el correo: en el límite entre el bien y el mal](#2.2)

 <a name="s2.2.1"></a>[2.2.1. El riesgo está subdividido en dos niveles](#2.2.1)

 <a name="s3"></a>[3. Consejos técnicos y encriptación](#3)

 <a name="s3.1"></a>[3.1. Para empezar: encriptar es mucho más fácil de lo que piensas](#3.1)
 <a name="s3.2"></a>[3.2. ¿Cómo funciona la encriptación?](#3.2)
 <a name="s3.3"></a>[3.3. ¿Qué aplicación debería usar para encriptar mi correo?](#3.3)
 <a name="s3.3.1"></a>[3.3.1. ¿Qué debería hacer para usar GPG? Muy fácil](#3.3.1)

 <a name="s3.3.2"></a>[3.3.2. ¿Cómo funciona realmente?](#3.3.2)

 <a name="s3.3.3"></a>[3.3.3. ¿Qué debería hacer con mi clave pública?](#3.3.3)

 <a name="s3.4"></a>[3.4. En pocas palabras](#3.4)
 <a name="s4"></a>[4. Para los más paranoicos: remailer anónimo](#4)

<a name="1"></a>

[1.1. Sobre la privacidad](#s1)

Las investigaciones policiales mencionan, cada vez más, frases proferidas por los acusados en relación a la posibilidad de codificar sus
cartas, y estas reflexiones se consideran como una sugerencia explícita de su voluntad de cometer actos ilegales. Con la misma frecuencia,
los archivos de la policía contienen fragmentos de mensajes de correo que han sido intervenidos

Desde la [intervención de la NSA](http://www.eff.org/legal/cases/att/) hasta el [caso más reciente de A/I](http://autistici.org/ai/crackdown/#es), y
a la [confiscación de servidores de Indymedia](http://www.indymedia.org.uk/en/actions/2004/fbi/), se hace presente un creciente y extendido
control. Estamos avanzando a grandes
pasos hacia un control masivo, en el que la privacidad colapsará en pos de la ley y el orden, entre paradigmas de sospecha y el falso
problema del terror.

En una situación en la que el control nos es siempre forzosamente presentado como la solución a todos los males, nos gustaría aclarar los
peligros implicados en la comunicación a través del correo electrónico y las contramedidas que puedes tomar.

<a name="1.1"></a>

[1.1. ¿Por qué debería encriptar mi correo?](#s1.1)

Si no sabes qué significa codificación, encriptación, etc., échale una mirada a esta página:
[https://es.wikipedia.org/wiki/Criptografía](https://es.wikipedia.org/wiki/Criptografía).

Un mensaje no encriptado enviado a través de Internet es como una postal sin sobre: el cartero, el portero, los vecinos y cualquier otra
persona que pueda ponerle sus manos encima leerá el mensaje que has escrito fácilmente

No nos cansaremos de repetir que usar la criptografía no solamente nos protege, sino que también protege la privacidad del destinatario.

<a name="1.1.1"></a>

[1.1.1. Secreto No-personal: negociaciones, finanzas, justicia](#s1.1.1)

Periodista, abogado, físico, contador: hay muchas occupaciones en las que por contrato, por ética o por ley se demanda, mantener el secreto
profesional. Más y más gente usa Internet por razones de trabajo, y aquellos que deben proteger los secretos de sus clientes están obligados
a encriptar sus correos si no desean que sus propuestas comerciales, archivos legales y de documentación se pierdan en los procesos
burocráticos de la Web.

Si no codifican sus documentos, estarán siendo negligentes con respecto a las medidas cuyo propósito es mantener el secreto profesional y se
estarán, también, arriesgando a consecuencias legales y financieras.

<a name="1.1.2"></a>

[1.1.4. Secretos personales: vida privada y sentimientos](#s1.1.2)

¿No encriptas tu correo porque no tienes nada que esconder? Vale. Entonces ¿Por qué cierras las cortinas en tu casa?

Ciertamente no te gustaría que algún extraño se sentara en el escritorio de tu proveedor de Internet y se riera al leer, por placer, los
mensajes que envías a tu mejor amigo. Si nunca has encriptado tu correo, es probable que algún extraño haya leído lo que has escrito...

<a name="2"></a>

[2. Todos los riesgos que sufre TU casilla de correos](#s2)

<a name="2.1"></a>

[2.1. La ruta de tus mensajes de correo electrónico](#s2.1)

Tu correo está expuesto a numerosos riesgos: Cuando envías un mensaje, tu cliente de correo contacta un servidor a través del protocolo SMTP
y transmite tu mensaje a ese servidor. Esta transmisión se encuentra generalmente en texto plano (sin ningún tipo de codificación). El
servidor SMTP contacta al servidor de destino, y esta transmisión también será llevada a cabo en texto plano.

Lo que es más, cada vez que envíes un correo, el ordenador del proveedor del servicio guardará una copia de tu e-mail. Veamos como
funciona:

<a name="2.1.1"></a>

[2.1.1. Las incontables copias de tus mensajes de correo electrónico](#s2.1.1)

Tus e-mails viajan a través de Internet dejando detrás cierto número de copias.

Los e-mails se desplazan a través de la Web con una secuencia de copias grabadas por un servidor de correo (ej. tu servicio proveedor de
Internet) en otro servidor de correo.

Si por ejemplo envías un correo desde tu casa en el norte de Roma a una persona que vive en el sur de Roma, tu correo será copiado al menos
tres veces:

1.  Tu ordenador (**copia original**) envía tu mensaje a un primer ordenador en las oficinas de tu proveedor (**1ª copia**);
2.  en la mejor de las hipótesis, el ordenador del proveedor envía su copia directamente al ordenador del proveedor de tu destinatario (**2ª
    copia**). Pero podría haber más pasos;
3.  el ordenador del ISP de tu destinatario guarda una copia de tu correo (**3ª copia**), mientras espera que alguien descargue su correo
    (**copia final**).

Entonces, sólo para viajar a través de algunos vecindarios, tu correo ha sido guardado al menos tres veces en dos discos duros diferentes (2
servidores de correo de proveedores de internet). Y detrás de estos discos duros se esconden firmas comerciales, técnicos de IT curiosos,
todo tipo de funcionarios y mucha otra gente... Además, estas tres copias son la mejor hipótesis: si miras los encabezados de tu correo
entrante (todos los clientes de correos tienen una opción para hacer esto), verás que los pasos son muchos más, habrá tantos como copias de
tu mensaje

Teóricamente, estas copias múltiples de tu correo deberían ser borradas por los proveedores luego de unas horas. Pero las nuevas leyes que
están siendo mundialmente sancionadas contra el "cibercrimen" permiten la retención de todas las copias durante varios meses, al menos en lo
que respecta a la información correspondiente al destinatario y remitente.

<a name="2.2"></a>

### [2.2. Leyendo el correo: en el límite entre el bien y el mal](#s2.2)

Cuando descargas y lees tu correo, generalmente usas uno de estos métodos:

1.  Por correo web, con un simple navegador.
2.  Con un cliente de correo, usando protocolos POP3 o IMAP.

En ambos casos, la mayor cantidad de tu correo viaja en texto plano, a menos que tomes las contramedidas necesarias para que tu contraseña y
tus mensajes no sean completamente legibles cuando pasen del proveedor a tu ordenador.

<a name="2.2.1"></a>

[2.2.1. El riesgo está subdividido en dos niveles](#s2.2.1)

Primero de todo, durante sus varios movimientos por la Web, un mensaje puede ser interceptado y leído. Segundo, los mensajes copiados en
servidores pueden ser leídos por cualquiera que pueda accedar a estas computadoras.

<a name="3"></a>

[3. Consejos técnicos y encriptación](#s3)

En relación al primer problema, puedes usar canales de comunicación codificada para enviar los mensajes a tus servidores (usando SMTP con
soporte SSL y POP3 o IMAP con SSL).

En cuanto al segundo problema, puede ser resuelto usando un programa para la encriptación de contenido de e-mails. Pero sé cuidadoso:
deberías usar un **programa open-source**, de otra manera no puedes estar seguro de que hará lo que dice que hace. Los programas de
encriptación pueden sufrir varios ataques: alguien podría querer crear una puerta trasera que le permitiera leer tu correo encriptado. Si el
programa que estás usando no es open-source, eso es si el código fuente no está disponible y no puede ser examinado, entonces un programador
resuelto a dar a todos la posibilidad de proteger su privacidad no podrá saber sobre la posible presencia de puertas traseras.

<a name="3.1"></a>

[3.1. Para empezar: encriptar es mucho más fácil de lo que piensas](#s3.1)

Si sabes como administrar un programa normal de correo (escribir e-mails, adjuntar archivos, etc.), entonces no deberías tener problemas en
administrar programas de encripción.

GPG, el programa open-source que nosotros (y muchos otros) te recomiendan, es bastante fácil de usar, también porque puede ser instalado a
través de un plugin ad-hoc en varios clientes de correo. En particular, sugerimos usar Thunderbird (cliente de correo) con Enigmail (plugin
de encriptación), ambos disponibles para cualquier sistema operativo.

De cualquier forma, el grado de seguridad que obtendrás depende del nivel de protección que puedas darle a tu clave privada, es decir, al
archivo que contiene el disparador de los mecanismos criptográficos. Si pierdes tu clave privada, la seguridad será falaz. Si quieres saber
qué es una clave privada, sigue leyendo.

<a name="3.2"></a>

[3.2. ¿Cómo funciona la encriptación?](#s3.2)

Hay dos categorías diferentes de métodos de encripción: simétrico y asimétrico.

Un método de encripción simétrico puede, por ejemplo, establecer una correspondencia entre letras y sus respectivos números:

A ---&gt; 1

B ---&gt; 2

C ---&gt; 3

etc,.

El inconveniente de este sistema es que tan pronto como descubres como ha sido codificado un mensaje, no habrá problema en decodificarlo.

Un sistema de encriptación asimétrico es mucho más sostificado. La codificación y decodificación toman lugar mediante dos mecanismos
diferentes: su diferencia es tal, que el mecanismo de encriptación puede hacerse público.

Aquí hay un ejemplo para entender mejor como funciona este sistema. Un agente secreto que viaja al exterior debe hacer reportes periódicos a
sus jefes. ¿Cómo puede enviar sus reportes? Fácilmente: antes de marcharse, el agente obtiene de su jefa una caja con 100 candados abiertos;
cuando necesita enviar un mensaje, lo pone en una caja sólida, cierra la caja con uno de los candados y la envía por correo. Una vez que ha
sido cerrada, la caja sólo puede ser abierta por los superiores del agente, quienes se han quedado en casa con la llave necesaria. ¿Cuáles
son los aspectos fundamentales de este sistema?

1.  Cuando el agente cierra la caja, nadie puede abrirla sin la respectiva llave, ni siquiera el propio agente.
2.  Si el agente es atrapado, la policía no puede decodificar nada, porque sólo pueden poner sus manos sobre el candado.
3.  El agente no tiene necesidad de esconder sus candados, ya que sólo pueden cerrar cajas, pero no pueden abrirlas.

Este ejemplo es sobre un agente secreto, pero la encriptación es recomendable independientemente de si tienes algo que esconder o no. Es
crucial demistificar la ecuación codificación = tener cosas ilegales que esconder. Y si lo piensas, esta es una ecuación que niegas cada
día, bien cuando cierras las cortinas, o bien cuando evitas miradas curiosas a tu monitor mientras escribes un correo.

Codificar solamente significa evitar que alguien (desde los empleados de tu proveedor a las empresas de marketing que monitorean la palabras
utilizadas en un mensaje para ajustar la producción de su compañía) lea lo que escribes, tus asuntos personales.

Moviéndonos hacia el mundo digital, llamamos clave pública al código de encripción (el candado), y clave privada al código de decodificación
(la llave).

Si usas un código de encriptación asimétrico, tendrás dos claves: una clave privada que tendrás que proteger con mucho cuidado y mantener a
salvo, y una clave pública, que puede estar disponibles para cualquiera, por ejemplo, en una página web apropiada.

<a name="3.3"></a>

### [3.3. ¿Qué aplicación debería usar para encriptar mi correo?](#s3.3)

PGP (Pretty Good Privacy) es un software que permite una comunicación totalmente reservada aún entre personas que nunca se han visto y viven
a miles de kilómetros de distancia. Esto es posible gracias a la encriptación de clave pública.

Desafortunadamente, las últimas versiones PGP no pueden considerarse seguras, en eso de que los usuarios no pueden verificar el código del
programa.

Esta es una de las razones por las que GPG (Gnu Privacy Guard) fue creado: GPG es un software muy similar a PGP que fue lanzado bajo una
licencia Gnu, por lo que su código puede ser verificado.

Para más detalles sobre PGP, visita:

 <https://es.wikipedia.org/wiki/Pretty_Good_Privacy>

Para más información sobre GPG, visita:

 <https://es.wikipedia.org/wiki/GNU_Privacy_Guard>

 <http://www.gnupg.org/>

<a name="3.3.1"></a>

#### [3.3.1. ¿Qué debería hacer para usar GPG? Muy fácil](#3.3.1)

Después de instalar Thunderbird y Enigmail en tu ordenador, todo lo que necesitas es un par de claves. Para crearlas, sigue estas
instrucciones:
[http://dragly.org/2010/02/13/getting-started-with-encrypted-e-mail-using-thunderbird-and-enigmail/](http://dragly.org/2010/02/13/getting-started-with-encrypted-e-mail-using-thunderbird-and-enigmail/)

<a name="3.3.2"></a>

[3.3.2. ¿Cómo funciona realmente?](#s3.3.2)

1.  Primero, deberías crear un par de claves pública-privada con tu software de encripción, luego deberías dar tus claves públicas a la
    gente con la que quieras comunicarte. Nunca deberías dar tu clave privada a nadie.
2.  Cuando escribes un correo, deberías encriptarlo con la clave pública de tu destinatario.
3.  El proceso de encriptación inserta una especie de “candado” electrónico en tu mensaje. Aún si tu correo ha sido intervenido mientras se
    desplazaba por la Web, no se podrá acceder a su contenido porque estaría faltando la clave.
4.  Cuando tu mensaje llega a su destino, tu destinatario pondrá una frase-clave (de más de una palabra). El software de encriptación usará
    la clave privada de tu destinatario para corroborar que se ha usado la correspondiente clave pública.
5.  Después el software usará la clave privada para abrir la encripción del mensaje permitiendo leer el correo.

¿Es fácil, no?

Puedes encontrar muchos tutoriales sobre Enigmail/GPG al buscar en internet

<a name="3.3.3"></a>

[3.3.3. ¿Qué debería hacer con mi clave pública?](#s3.3.3)

Ya estará claro que es **fundamental** difundir tu clave pública lo máximo posible para usar la encriptación asimétrica: si nadie tiene tu
clave pública, nadie podrá enviarte mensajes codificados.

Uno de los servidores que puedes usar para publicar tu clave pública es:
 http://pgpkeys.mit.edu/

<a name="3.4"></a>

[3.4. En pocas palabras](#s3.4)

Utiliza un software para escribir tus mensajes privados, y configura tu cliente de correo de tal manera que envíe y reciba mensajes de forma
segura (SMTP con soporte SSL; POP3 o IMAP con SSL).

Si quieres saber más, te recomendamos:

- <https://autistici.org/crypto>
- <http://freehaven.net>
- <http://enigmail.mozdev.org>
- <http://www.gnupg.org>

<a name="4"></a>

[4. Para los más paranoicos: remailer anónimo](#s4)

La criptografía sí tiene un problema: ya que debes publicar tu clave publica en un servidor para que esté disponible, el solo hecho de que
tu dirección pueda ser conectada a un nombre desafía el concepto de privacidad. Es por esto que han sido creadas una serie de herramientas
conocidas como remailer anónimo: de hecho, estas herramientas pueden esconder por completo el nombre del remitente.

Una buena referencia sobre el asunto es [Preguntas frecuentes/Andre Bacard](http://www.andrebacard.com/remail.html). Generalmente hablando,
los remailers anónimos son servidores que funcionan como mediadores entre los remitentes y los destinatarios en la etapa de entrega del
correo: así, el verdadero remitente es reemplazado por el mediador. Al pasar entre una cadena de varios mediadores, obtienes un grado de
seguridad decente y se puede argüir que tu correo ha sido enviado anonimamente.

Para hacer un uso inteligente de esta herramienta, deberías pasar a través de varios remailers anónimos. Si quieres obtener un buen nivel de
seguridad, necesitas primero codificar tu mensaje y luego enviarlos a través de una cadena de remailers. Los remailers pueden ser usados a
través de varios clientes.

Aquí puede encontrarse una lista de clientes:
[http://www.autistici.org/crypto](http://www.autistici.org/crypto/index.php/remository/Remailer-clients/)

A esta altura, el soporte para los protocolos SSL o TLS está embebido en casi todos los servidores y clientes de correo. Ambos protocolos
añaden un nivel criptográfico que previene, por ejemplo, que tu contraseña, viaje en texto plano a través de internet. Habilitarlos en un
cliente es relativamente fácil, pero no todos los proveedores los admiten.

Nuestro [howto sobre correo](/services/mail#howto) ofrece varios manuales explicando cómo configurar un cliente de correo para usar
SSL/TLS con A/I, mientras que nuestra <u>[página sobre remailers](/docs/anon/remailer)</u> ofrece más información
sobre remailers anónimos.

De todas formas, hay una lección que aprender del caso de A/I mencionado anteriormente, cuando la policía postal usó la excusa de una
investigación sobre terrorismo para confiscar secretamente el contenido de nuestro servidor, con la ayuda del hosting de los servidores:
esto nos enseña una vez más que no es seguro confiar en otra gente la privavidad individual, y que la privacidad en Internet no es muy
diferente de la privacidad que tenemos al caminar por las calles de este mundo falsamente libre.

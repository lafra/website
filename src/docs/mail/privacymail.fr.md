title: Protéger sa vie privée : Comment et pourquoi ?
----

Protéger sa vie privée : Comment et pourquoi ?
==============================================

Sommaire
-------------------

 <a name="s1"></a>[1. A propos de la vie privée](#1)

 <a name="s1.1"></a>[1.1. Pourquoi devrais-je chiffrer mes e-mails ?](#1.1)

 <a name="s1.1.1"></a>[1.1.1. Secret professionnel : négociations, finance, justice](#1.1.1)

 <a name="s1.1.2"></a>[1.1.2. Secret personnel : vie privée et sentiments](#1.1.2)

 <a name="s2"></a>[2. Tous les risques de VOTRE boîte mail](#2)

 <a name="s2.1"></a>[2.1. Le chemin emprunté par vos e-mails](#2.1)

 <a name="s2.1.1"></a>[2.1.1. Le nombre de copies de vos e-mails](#2.1.1)

 <a name="s2.2"></a>[2.2. Lire les e-mails : entre bien et mal](#2.2)

 <a name="s2.2.1"></a>[2.2.1. Deux niveaux de risque](#2.2.1)

 <a name="s3"></a>[3. Astuces techniques et Chiffrement](#3)

 <a name="s3.1"></a>[3.1. Pour commencer : chiffrer un message est plus facile que vous ne le pensez](#3.1)
 <a name="s3.2"></a>[3.2. Comment le chiffrement fonctionne ?](#3.2)
 <a name="s3.3"></a>[3.3. Quelle application devrais-je utiliser pour chiffrer mes e-mails ?](#3.3)
 <a name="s3.3.1"></a>[3.3.1. Que devrais-je faire pour utiliser GPG ? C'est très simple...](#3.3.1)

 <a name="s3.3.2"></a>[3.3.2. Mais comment le faire fonctionner concrètement ?](#3.3.2)

 <a name="s3.3.3"></a>[3.3.3. Que puis-je faire avec ma clé publique ?](#3.3.3)

 <a name="s3.4"></a>[3.4. En quelques mots](#3.4)

<a name="1"></a>

[1.1. A propos de la vie privée](#s1)

De plus en plus souvent, les enquêtes policières mentionnent des phrases prononcées par les accusés à propos de la possibilité d'encoder
leurs lettres, et considèrent de fait qu'ils supportent des actes illégaux. De même, les fichiers de police contiennent des extraits
d'e-mails.

De l'[affaire NSA](https://www.eff.org/nsa-spying) aux nombreuses [enquêtes](/who/collective) que A/I a du affronter, il
a été prouvé que notres vies et notres communications sont surveillés.

Nous avançons à grands pas vers un contrôle de masse, où la vie privée va disparaître au profit d'un amour de la loi et de l'ordre, où la
suspicion et les soupçons de menace vont régner.

Dans un monde où le contrôle forcé est présenté comme la solution à tous les maux, nous aimerions éclaircir certaines choses, comme les
dangers impliqués par la communication électronique et les mesures que vous pouvez prendre pour les contrer.

<a name="1.1"></a>

[1.1. Pourquoi devrais-je chiffrer mes e-mails ?](#s1.1)

Si vous ne connaissez pas les notions de chiffrement, cryptage, etc., vous devriez jeter un oeil à [cette page](https://fr.wikipedia.org/wiki/Cryptographie).

Un e-mail non chiffré envoyé via Internet est comme une carte postale sans enveloppe : les employés de la poste, les concierges, les voisins
ou n'importe qui d'autre qui peut mettre la main dessus va aisément lire le message que vous avez écrit.

Nous ne répéterons jamais assez que le fait d'utiliser le chiffrement ne vous protège pas uniquement vous, mais aussi la vie privée de vos
contacts.

<a name="1.1.1"></a>

[1.1.1. Secret professionnel : négociations, finance, justice](#s1.1.1)

Les journalistes, les avocats, les physiciens, les médecins, les banquiers : il y'a tant de professions qui sont liés par un contrat de
confidentialité, par l'éthique ou par la loi de garder le secret professionnel. De plus en plus de personnes utilisent Internet pour des
raisons professionnelles, et ceux qui veulent protéger les secrets de leurs clients sont obligés de chiffrer leurs e-mails si ils ne veulent
pas que leurs propositions commerciales, documents juridiques, ou autres documents sensibles soient perdus dans la Toile.

S'ils n'encodent pas leurs documents, ils vont négliger les mesures nécessaires pour garder le secret professionnel et ils risquent d'être
responsables légalement et financièrement.

<a name="1.1.2"></a>

[1.1.4. Secret personnel : vie privée et sentiments](#s1.1.2)

Vous ne chiffrez pas vos messages parce que vous n'avez rien à cacher ? Très bien, mais alors pourquoi fermez vous les rideaux ou la porte
chez vous ?

Vous ne voudriez certainement pas qu'un étranger présent dans le bureau de votre fournisseur d'accès à Internet sourit lorsqu'il lit, pour
le plaisir, les messages que vous êtes en train d'envoyer à votre meilleur(e) ami(e). Si vous n'avez jamais chiffré vos e-mails, c'est un
peu comme si des étrangers pouvaient avoir lu tout ce que vous aviez écrit...

<a name="2"></a>

[2. Tous les risques de VOTRE boîte mail](#s2)

<a name="2.1"></a>

[2.1. Le chemin emprunté par vos e-mails](#s2.1)

Votre e-mail est exposé à divers risques : quand vous envoyez un message, votre client de messagerie contacte un serveur grâce à un
protocole appelé SMTP et transmet votre message à ce serveur. Cette transmission est généralement réalisée en text clair (sans aucun
chiffrement). Le serveur SMTP contacte à son tour le serveur de destination, et cette transmission va être envoyée en text clair encore.

Enfin, chaque fois que vous envoyez un e-mail, les ordinateurs du fournisseur d'accès à Internet de vous utilisez va enregistrer une copie
de votre message. Voyons voir comment cela marche:

<a name="2.1.1"></a>

[2.1.1. Le nombre de copies de vos e-mails](#s2.1.1)

Votre e-mail voyage à travers Internet en laissant derrière un certain nombre de copies de ce message.

Les messages passent à travers le Web avec une séquence de copies enregistrées par un serveur de messagerie (exemple : votre F.A.I --
Fournisseur d'Accès à Internet) dans un autre serveur de messagerie.

Si, par exemple, vous avez envoyé un mail de votre maison dans le nord de Paris à une personne qui vit dans le sud de Paris, votre mail va
être copié au moins trois fois (dans le meilleur des cas) :

1.  Votre ordianteur (**copie originale**) envoie votre message à un premier ordinateur au bureau de votre fournisseur de messagerie (1ère
    copie);
2.  dans le meilleur cas possible, le fournisseur envoie sa copie directement à l'adresse du fournisseur de messagerie de votre contact
    (**2ème copie**). Mais il peut y avoir plus d'étapes;
3.  votre F.A.I peut lui aussi garder une copie de votre mail (**3ème copie**), en attendant que quelqu'un télécharge le mail (**fin de la
    copie**).

Donc, dans son voyage dans la même cité, votre mail a été enregistré au moins 3 fois dans 2 différents disques durs, et chaque fois en une
copie parfaite. Et derrière ces disques durs, se cachent des firmes commerciales, des techniciens en informatique, toutes sortes d'officiels
et bien d'autres personnes.. Ces trois copies sont le meilleur cas possible : si vous regardez la tête de vos emails entrants (tous les
clients de messagerie ont cette option), vous allez voir les étapes qui sont bien plus nombreuses que celles décrites précédemment, en
fonction du nombre de copies de votre message.

Théoriquement, ces multiples copies de votre mail devrait être supprimées en moins de quelques heures par chaque F.A.I. Mais les nouvelles
lois qui apparaissent dans le monde dans le but de la lutte contre le "cybercrime" imposent l'enregistrement de toutes ces copies pour
plusieurs mois, au minimum les parties qui signalent l'émetteur et le destinataire.

<a name="2.2"></a>

[2.2. Lire les e-mails : entre bien et mal](#s2.2)

Quand vous téléchargez et lisez votre e-mail, vous utilisez générallement une de ces méthodes :

1.  Via une messagerie Internet, avec votre navigateur Internet.
2.  Via un client de messagerie, en utilisant le protocole POP3, ou le protocole IMAP.

Dans tous les cas, le contenu de votre mail voyage toujours en text clair, à moins que vous ne preniez certaines mesures sans lesquelles
votre mot de passe et vos messages sont totalement lisible quand ils voyagent de votre fournisseur de messagerie à votre ordinateur.

<a name="2.2.1"></a>

[2.2.1. Deux niveaux de risque](#s2.2.1)

Premièrement, pendant ces nombreux mouvements au sein du Web, un message peut être intercepté et lu. Deuxièmement, les messages copiés dans
les serveurs peuvent être lus par n'importe qui qui a accès à ces serveurs.

<a name="3"></a>

[3. Astuces techniques et Chiffrement](#s3)

Pour régler le premier problème, vous pouvez utiliser des canaux de communication cryptés pour envoyer vos messages à vos serveurs (en
utilisant SMTP avec support SSL et POP3 ou IMAP + SSL).

Pour le second problème, cela peut être résolu en utilisant un programme pour le chiffrement du contenu d'e-mail. Mais soyez prudents : vous
devez absolument utiliser un **programme open-source**, sinon vous ne pouvez pas être sûr que le programme fasse ce qu'il est censé faire.
Les programmes de chiffrement peuvent actuellement être victime de plusieurs failles : quelqu'un pourrait vouloir créer une porte dérobée
l'autorisant à lire un message chiffré. Si le programme que vous utilisez n'est pas open-source, c'est à dir que le code source n'est pas
disponible et ne peut pas être examinés, il empêche par conséquent un programmeur de garantir à tout le monde que le programme ne comporte
pas de portes dérobées.

<a name="3.1"></a>

[3.1. Pour commencer : chiffrer un message est plus facile que vous ne le pensez](#s3.1)

Si vous savez comment utiliser un programme de messagerie classique (écrire des messages, insérer des pièces jointes...), alors vous n'aurez
aucun problème à employer un programme de chiffrement !

GPG est le programme open-source de chiffrement que nous (et aussi beaucoup d'autres) vous recommandons. Il est actuellement le plus simple
à utiliser, car il peut être installé grâce à un simple plugin sur différents clients de messagerie. En particulier, nous vous recommandons
Thunderbird (client de messagerie) avec Enigmail (plugin de chiffrement), qui sont tous deux utilisables pour tous les systèmes
d'exploitation (Mac, PC, Linux...).

Le niveau de sécurité que vous allez avoir dépend du niveau de protection que vous allez assurer à votre clef privée : il s'agit du fichier
contenant le mecanisme cryptographique nécessaire au chiffrement. Si vous perdez votre clef privée, la sécurité va être illusoire. Si vous
voulez savoir ce qu'est une clef privée, lisez le texte suivante.

<a name="3.2"></a>

[3.2. Comment le chiffrement fonctionne ?](#s3.2)

Il y'a deux différentes catégories de méthode de chiffrement : symétrique et asymétrique.

Un chiffrement symétrique peut, par exemple, établir une correspondance entre des lettres et leurs nombres respectifs :

A ---&gt; 1

B ---&gt; 2

C ---&gt; 3

etc.

L'inconvénient de ce système est que dès que vous avez découvert comment un message a été chiffré, il n'y aura aucun problème pour le
déchiffrer.

Un système asymétrique est beaucoup plus sophistiqué. Le chiffrement et le déchiffrement sont deux mécanismes distincts : leur différence
est que le mécanisme de chiffrement peut être rendu public.

Voici un exemple pour mieux comprendre comment ce système marche. Un agent secret à l'étranger doit périodiquement envoyer un rapport à ses
chefs. Comment peut-il envoyer ses rapports ? Facile : avant de partir à l'étranger, l'agent secret obtient 100 cadenas ouverts de la part
de son chef (chouette cadeau, non ?); quand il a besoin d'envoyer un rapport, elle le met dans une solide boîte, ferme la boîte avec un des
cadenas et l'envoie par mail. Une fois fermée, la boîte peut seulement être ouverte par les supérieurs, qui sont restés chez eux avec la clé
correspondante. Quels sont les aspects fondamentaux de ce sytème ?

1.  Quand l'agent ferme la boîte, personne ne peut l'ouvrir sans la clé respective, pas même l'agent lui-même.
2.  Si l'agent est attrapé, la police ne peut pas déchiffrer quoique ce soit, puisqu'ils ne peuvent pas ouvrir la boîte.
3.  L'agent n'a pas besoin de cacher ses cadenas, car ils peuvent seulement fermer la boîte mais ne peuvent pas l'ouvrir.

Cet exemple est à propos d'un agent secret, mais le chiffrement est recommandé même si vous n'avez rien de particulier à cacher. Il est
crucial de démystifier l'utilisation du chiffrement et de casser cette impression de protection de sujets illégaux à cacher. Et si vous
réfléchissez à propos de ça, c'est quelque chose que vous refusez chaque jour, quand vous fermez vos ridaux ou que vous vous protégez des
regards curieux sur votre moniteur lorsque vous écrivez un message.

Chiffrer signifie juste que vous ne voulez pas que quelqu'un lise tout ce que vous écrivez, vos affaires personnelles.

Si on transpose le cas de l'agent secret à la situation réelle, on appelle "clé publique" le moyen de chiffrer les messages (le cadenas), et
"clé privée" le moyen de déchiffrer le message (la clé).

Si vous utilisez un code de chiffrement asymétrique, vous allez avoir deux clés : une privée que vous devrez protéger très précieusement et
garder absolument en sécurité, et une clé publique, qui peut être donnée à n'importe qui, sur un site web par exemple.

<a name="3.3"></a>

[3.3. Quelle application devrais-je utiliser pour chiffrer mes e-mails ?](#s3.3)

PGP (Pretty Good Privacy) est un logiciel qui permet des communications chiffrées entre deux personnes qui ne se sont jamais vu l'un l'autre
et qui vivent à des milliers de kilomètres l'un de l'autre. Grâce à la clé publique, c'est possible.

Malheureusement, la dernière version de PGP ne peut pas être considéré comme sécurisée, car les utilisateurs ne peuvent pas vérifier le code
source.

C'est une des raisons pour lesquelles GPG (Gnu Privacy Guard) a été crée : GPG est un logiciel très similaire à PGP qui est publiée sous une
license GNU, et dont le code source peut ainsi être vérifié.

Pour plus de détails à propos de PGP :

- <https://fr.wikipedia.org/wiki/Pretty_Good_Privacy>

Pour plus de détails à propos de GPG :

- <https://fr.wikipedia.org/wiki/GNU_Privacy_Guard>
- <http://www.gnupg.org/>

<a name="3.3.1"></a>

[3.3.1. Que devrais-je faire pour utiliser GPG ? C'est très simple...](#3.3.1)

Après avoir installé Thunderbird et Enigmail sur votre ordinateur, tout ce dont vous avez besoin est une paire de clefs. Pour les créer,
suivez ces instructions:
[http://dragly.org/2010/02/13/getting-started-with-encrypted-e-mail-using-thunderbird-and-enigmail"](http://dragly.org/2010/02/13/getting-started-with-encrypted-e-mail-using-thunderbird-and-enigmail/)

<a name="3.3.2"></a>

[3.3.2. Mais comment le faire fonctionner concrètement ?](#s3.3.2)

1.  Premièrement, vous allez devoir créer un duo clé publique/clé privée avec votre logiciel de chiffrement, ensuite vous pourrez donner vos
    clés publiques aux personnes avec qui vous voulez communiquer. Vous ne devrez jamais donner votre clé privée à quelqu'un d'autre,
    quelqu'il soit !
2.  Quand vous écrivez votre e-mail, vous devrez chiffrer le message avec la clé publique de votre desetinataire.
3.  Le processus de chiffrement insert une sorte de verrou électronique dans votre message. Même si votre e-mail était découvert à travers
    le Web, son contenu ne serait pas accessible car il manquerait la clé.
4.  Quand votre message arrive à destination, votre destinataire va entrer une phrase de passe (comme un mot de passe, mais plus compliqué).
    Le logiciel de chiffrement va ensuite utiliser la clé privée de votre destinataire pour certifier que la clé publique
    utilisée corresponde.
5.  Enfin, le logiciel va utiliser la clé privée de votre destinataire pour déverrouiller le message et autoriser la lecture du message.

Facile, non ?

Vous pouvez trouver de nombreux tutoriels sur Internet à l'aide des termes "engimail"/"GPG"

Un tutoriel complet est présent ici (en Anglais) : https://www.enigmail.net/documentation/quickstart.php

<a name="3.3.3"></a>

[3.3.3. Que puis-je faire avec ma clé publique ?](#s3.3.3)

Ce doit être à présent clair qu'il est **fondamental** de publier votre clé publique autant que possible dans le cadre de l'utilisation d'un
chiffrement asymétrique : si personne n'a votre clé publique, personne ne va être capable de vous envoyer des messages chiffrés !

Un des serveurs où vous pouvez publier vos clés publiques est:  http://pgpkeys.mit.edu/

<a name="3.4"></a>

[3.4. En quelques mots](#s3.4)

Utiliser un logiciel de chiffrement pour écrire vos messages privés, et configurer votre client de messagerie (Thunderbird ici) pour envoyer
e recevoir vos messages de façon sécurisée (SMTP avec suppor SSL; POP3 ou IMAP avec SSL).

Pour en apprendre plus :

- <https://autistici.org/crypto>
- <http://freehaven.net>
- <http://enigmail.mozdev.org>
- <http://www.gnupg.org>


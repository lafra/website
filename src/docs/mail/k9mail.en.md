title: How to configure K-9 mail on Android
----

How to configure K-9 mail on Android
==================================================

Chose to configure a new account.

![](/static/img/man_mail/fr/k92.jpg)

It will be displayed by default if this is your first account.

If not, you can add a new account using the “options” key/command and “Add a new account”.

You will then get on this page, where you will have to fill both the field with your Autistici e-mail and your password (be aware that the
domain name may not be the same, it will depend on what you have chosen.

On the next step, you will have to choose which type of mail protocol you want to use.

![](/static/img/man_mail/fr/k93.jpg)

IMAP would be appropriate if your Android terminal is not your main mail client: it will only consult data on the mail server and operate
directly on it.

As for POP3, it creates a copy of your mail locally and you operate on that copy. It permits you to keep your mail regardless of their
presence on the mail server.

IMAP (ingoing mail parameters)
------------------------------

![](/static/img/man_mail/fr/k94.jpg)

To configure K-9 as an IMAP mail client, you will have to fill the next things:

 *Your username:*
 It simply is your email address. Be aware that K-9 automatically deletes the domain name after your ID. You will have to add it to the
 “Username” field.
 
 *Your password:*
 You shouldn’t have to fill that again since K-9 automatically retrieves it.
 
 *IMAP server:*
 Autistici’s IMAP server is the following: “mail.autistici.org”.
 
 *Security type:*
 You will have to use SSL with K-9, since TLS does not seem to be working. Select “SSL/TLS (always)”
 
 *Authentication type:*
 Leave the default, settings, which should be PLAIN.

 *Port:*
 Use 993, default port fort IMAP SSL.
 *Leave auto-detecting as it is.*
 *For the use of compression on the network, it depends on you.*
 
Note that all the parameters for Autistici are displayed [here](/docs/mail/connectionparms).

POP3 (ingoing mail parameters)
------------------------------

![](/static/img/man_mail/fr/k95.jpg)

To configure K-9 as an POP3 mail client, you will have to fill the next things:
 *Your username:*
 It simply is your email address. Be aware that K-9 automatically deletes the domain name after your ID. You will have to add it to the
 “Username” field.
 
 *Your password:*
 You shouldn’t have to fill that again since K-9 automatically retrieves it.
 
 *POP3 server:*
 Autistici’s POP3 server is the following: “mail.autistici.org”.

*Security type:*
 You will have to use SSL with K-9, since TLS does not seem to be working. Select “SSL/TLS (always)”.

*Authentication type:*
 Leave the default, settings, which should be PLAIN.

*Port:*
 Use 995, default port for POP3 SSL.

SMTP (outgoing mail parameters)
-------------------------------

![](/static/img/man_mail/fr/k96.jpg)

To configure outgoing mail on K-9, do next:

 *SMTP server:*
 Autistici’s POP3 server is the following: “mail.autistici.org”.
 
 *Security type:*
 You will have to use SSL with K-9, since TLS does not seem to be working. Select “SSL/TLS (always)”
 
 *Port:*
 Use 465, default port for SMTP SSL.
 *“Authentication required” should be filled.*
 
 *Authentication type:* Leave the default, settings, which should be AUTOMATIC.
 
 *Your username:*
 It simply is your email address. Be aware that K-9 automatically deletes the domain name after your ID. You will have to add it to the
“Username” field.

*Your password:*
 You shouldn’t have to fill that again since K-9 automatically retrieves it.

title: Roundcube Webmail Howto 
----

Roundcube Webmail Howto
===========================

- [How to access the webmail](#login)
- [How to add addresses to my addressbook](#rubrica1)
- [How to send messages to more than one address](#rubrica2)
- [I cannot find "sent-mail" or "draft" folder](#cartelle)

**Note:** most screenshot in this howto are in Italian, we are very busy and still did not made them available in English. If you have time
and want to help us, please do the missing screenshot and send them to us! You will help us make this howto better!

<a name="login"></a>

Access your User Panel and Webmail
----------------------------------

To access your webmail login from [A/I homepage](/) as shown in the picture:

![](/static/img/man_mail/it/login.jpg)

Now you entered your User Panel. You can read your mail clickin on both your mailbox address or the small mail icon.

![](/static/img/man_mail/en/roundcube01-readmail.jpg)

<a name="rubrica1"></a>

Address Book
------------

To add a new contact to your addressbook click on *Addressbook* in the top-right corner and then on the *Add a contact* icon (the one with
the small green-circled *+* sign).

![](/static/img/man_mail/it/rubrica_vuota.jpg)

Fill in all the fields you deem necessary and then click on *Save*

![](/static/img/man_mail/it/primo_contatto.jpg)

![](/static/img/man_mail/it/come_compare.jpg)

![](/static/img/man_mail/it/inserire_contatto.jpg)

<a name="rubrica2"></a>

**To send messages to more than one address** click on *Address book* in the top-right corner and select more than one contact while keeping
your **CTRL** key pressed (it's a shortcut for multiple selections!).

Then click on the *New Message* icon and all of your contacts will be there!

![](/static/img/man_mail/it/destinatari.jpg)

![](/static/img/man_mail/it/seleziona_indirizzi.jpg)

![](/static/img/man_mail/it/invia_mail_con_piu_indirizzi_.jpg)

<a name="cartelle"></a>

Folders
-------

**If you cannot see the *Sent* folder or the *Draft* folder** first of all check the *Setting* menu in the top-right corner of the page: go
into the *Folders* tab and ensure that their respective checkbox is marked.

![](/static/img/man_mail/it/mancano_cartelle_1.jpg)

![](/static/img/man_mail/it/mancano_cartelle_2.jpg)

![](/static/img/man_mail/it/mancano_cartelle_3.jpg)

If the *Sent* or *Draft* folder do not exist, you can create them by using the *Create Folder* space at the bottom of the
*Settings-&gt;Folders* tab.

![](/static/img/man_mail/it/mancano_cartelle_4.jpg)

As you can see the folders are now where they should be!

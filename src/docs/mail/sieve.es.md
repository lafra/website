title: Cómo configurar filtros en tu correo web 
----

Cómo configurar filtros en tu correo web.
================================================

¿Qué es un filtro?
------------------

Desde julio de 2012 el sistema de correo web de A/I permite a todos los usuarios la creación de filtros.

Un filtro es una regla que permite a un usuario mover, copiar, borrar mensajes en su casilla dependiendo del remitente, asunto, tamaño u
otras categorías que se te ocurran.

Encontrarás una interfaz web para administrar tus filtros personales en tu correo web, luego de iniciar sesión con tu nombre de usuario y
contraseña en nuestra [página principal](/)

¿Quién necesita filtros?
------------------------

Los filtros pueden ser usados por cualquiera, pero podemos pensar en, al menos, dos tipos de usuarios que **REALMENTE** los necesitan.

Primero, aquellos que acceden a su correo en la WEB: los filtros brindan la posibilidad de organizar su correo en carpetas y subcarpetas
muy fácilmente.

Segundo, aquellos que acceden a su correo utilizando exclusivamente un cliente POP de correo: estos usuarios necesitarán al menos un filtro
para poder ver el correo que envíamos directamente a la carpeta "Basura" de sus casillas.

¿Cómo puedo agregar un filtro?
------------------------------

![](/static/img/man_mail/en/sieve.png)

1.  Accede a tu correo web.
2.  Ve a la sección "Configuración".
3.  Ve a la sección "Filtros".
4.  Haz click en el ícono "Agregar Filtro".
5.  Elige un nombre sencillo para tu filtro (que dé cuenta del criterio que estés usando para ordenar los mensajes)
6.  En el primer menú desplegable elige el encabezado que estés usando para ordenar los mensajes: remitente, asunto, tamaño, etc.
7.  En el segundo menú desplegable elige qué debería verificarse de ese encabezado: contiene una palabra, es igual a la palabra, etc.
8.  En la casilla que está al lado de los menúes, escribe la palabra que quieras buscar en el encabezado.
9.  Pincha en el botón Agregar.
10. Elige lo que deseas que se haga con los mensajes que estén dentro del criterio del filtro: ser movidos a otra carpeta; borrados;
    reenviados a cierta dirección de correo, etc.
11. Pincha en el botón Guardar.

¿Cómo agregar un filtro para leer la carpeta "Basura" a través de un cliente de correo POP
------------------------------------------------------------------------------------------

Sigue las instrucciones del párrafo anterior sobre cómo agregar un filtro.

1.  Elige un nombre común para este filtro: *retrobasura*
2.  **La regla debería encontrar mensajes cuyos encabezados "X-Spam-Flag" sean igual a "SI" y debería mover esos mensajes a la carpeta
    "ENTRADA" nuevamente.**
3.  Elige "\[...\]" en el primer menú desplegable.
4.  Escribe en los recuadros de texto que aparecieron de la nada: "X-Spam-Flag" y "SI" respectivamente
5.  En el segundo menú desplegable elige la opción "es igual a".
6.  Haz click en el botón Agregar
7.  Haz click en el botón Guardar

Si todo ha salido bien deberías poder ver algo similar a la siguiente imagen.

![](/static/img/man_mail/en/sieve-retrospam.png)
